# ANTology

ANTology is a program for simulation and optimization, based on a heavily extended Ant Colony Optimization algorithm.

## Simulation

ANTology models processes in a directed graph structure, where each node represents a single or multiple time unit process, and edges define the connectivity of the graph. Nodes can be "mandatory" or "optional", where, for example data output is mandatory, but user alert is optional in the sense that it does not have to be triggered, e.g. during normal operations.

Each node and edge can be customized to process and alter data.

## Ant Colony Optimization (ACO)

The classic application for ACO is the Travelling Salesman Problem: a salesman wants to vistin a number of cities, and he wants to know in which order, to minimize his travel time. While this problem can be solved for a few cities, the number of possible permutations increases with the factorial, e.g. for 15 cities, there are 10^12 possibilities.

ACO works by first defining a graph connecting the cities, where the distances are stored in the edges. Then, an "ant" explores the graph, and wanders off randomly untill all cities were visited. It does that repeatedly, and whenever it finds a shorter path, it leaves a trail of "pheromones". If, in the next round, the ant chooses an edge out of a node, it prefers the one with the highest pheromones. Over time, this leads to a near optimal solution.

## Restrictions and multi-variable optimization

ANTology extends this algorithm with the concept of multi-variable optimization: rather than optimizing for the shortest path (time), it can do that by restricting other variables, e.g. cost. Strictly speaking, this is a limitation that is imposed, e.g. it finds the optimal path, but is not allowed to overrun the total cost that it accumulates (in the TSP, this might be the fee for hotels, etc., that vary from city to city).

## Programmatic extensions using lua

For more complex tasks, ANTology allows for lua scripts to be executed whenever a node is visited. Lua has access to all internally defined variables at the given time, performs operations on these variables, and returns the results.

## Multi-ant search

ANTology has no limit on how many ants can search in parallel. This is particularly useful for applications with multiple concurrent agents.


## Graph editor  

ANTology comes with a vusial and interactive graph editor.


![Scheme](images/screenshot.png)  

