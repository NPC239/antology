/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

/**
 *
 * @author manfred
 */
public class AttributeList {
    private Attribute[] m_attr;
    private int m_count;
    
    AttributeList() {
        m_attr = new Attribute[256];
        m_count = 0;
    }
    
    void add(Attribute a) {
        m_attr[m_count] = a;
        m_count++;
    }

    void setSize(int n) {
        m_count = n;
        for (int i=0; i<n; i++)
            m_attr[i] = new Attribute();
    }
    
    void set(int i, String name, String value, String type, String comment) {
        m_attr[i].name = name;
        m_attr[i].value = value;
        m_attr[i].type = type;
        m_attr[i].comment = comment;
    }
    
    void add(String name, String value, String type, String comment) {
        Attribute a = new Attribute();
        
        a.name = name;
        a.value = value;
        a.type = type;
        a.comment = comment;
        
        add(a);
        
    }
    
    int count() {
        return m_count;
    }
    
    void clear() {
        m_count = 0;
    }
    
    Attribute get(int i) {
        return m_attr[i];
    }

    void remove(int idx) {
        m_count--;
        for (int i=idx; i<m_count; i++)
            m_attr[i] = m_attr[i+1];
    }
    
}
