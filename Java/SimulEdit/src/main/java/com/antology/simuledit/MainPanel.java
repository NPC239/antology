/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import java.io.File; 
import java.util.Scanner; 

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.json.simple.parser.ParseException;

/**
 *
 * @author manfred
 */
public class MainPanel {
 
    private JFrame frame;
    private Drawing drawing;
    JScrollPane scroll;
    Settings m_settings;
    

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new MainPanel()::createAndShowGui);
    }

    
    private void createAndShowGui() {
        
        frame = new JFrame(getClass().getSimpleName());

        drawing = new Drawing();

        scroll = new JScrollPane(drawing);

        frame.add(scroll);

        drawing.SetScroll(scroll);
        //scroll.isVisible();
        
        
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        PanelListener listener = new PanelListener();

        drawing.addMouseListener(listener);
        drawing.addMouseMotionListener(listener);
        drawing.SetMouse(listener);
        drawing.setParent(this);
        
        
        m_settings = new Settings();
        m_settings.read();
        

        //HTML2Text ht = new HTML2Text();
        //String html = ht.toHTML("Testing\ntesting\ntesting\n");
        //String text = ht.toText(html);
        
        
        //drawing.load();
    }

    
    private void createAndShowGuiNew(String fileName) {
        
        frame = new JFrame(getClass().getSimpleName());

        drawing = new Drawing();

        scroll = new JScrollPane(drawing);

        frame.add(scroll);

        drawing.SetScroll(scroll);
        //scroll.isVisible();
        
        
        frame.pack();
        frame.setVisible(true);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        PanelListener listener = new PanelListener();

        drawing.addMouseListener(listener);
        drawing.addMouseMotionListener(listener);
        drawing.SetMouse(listener);
        drawing.setParent(this);
        
        if (fileName == null || fileName.equals(""))
            drawing.load();
        else
            drawing.loadData(fileName);
            
        drawing.doUpdate();
    }

    
    /*
    class LocalMenuListener implements MenuListener {

        @Override
        public void menuSelected(MenuEvent e) {
            System.out.println("menuSelected");
        }

    @Override
    public void menuDeselected(MenuEvent e) {
        System.out.println("menuDeselected");
    }

    @Override
    public void menuCanceled(MenuEvent e) {
        System.out.println("menuCanceled");
    }
    }*/

    
    
    
    
    class Drawing extends JPanel {

        PanelListener listener;

        SimpleNode m_nodes[];
        int m_nodesUsed;
        int m_selected;
        int m_selectedEdge = -1;

        SimpleEdge m_edges[];
        int m_edgesUsed;
        int m_fromNode = -1;
        
        SourceCode m_code;
                    
        MainPanel m_parent;
        NodeTypes m_types;
        
        private boolean m_havePath = false;
        
        Drawing() {
            m_nodes = new SimpleNode[512];
            m_edges = new SimpleEdge[512];
            m_nodesUsed = 0;
            m_selected = -1;
            addNode(90, 90);
            m_edgesUsed = 0;
            
            JMenuBar menubar = new JMenuBar();
            //setLayout(new BorderLayout());
            //add(menubar, BorderLayout.NORTH);

              
            JMenuBar b = new JMenuBar();
	    JMenu menu = new JMenu("Menu");
	    b.add(menu);
	    JMenuItem item = new JMenuItem("Menu Item");
	    menu.add(item);
	    setLayout(new BorderLayout());
	    add(b, BorderLayout.NORTH);
            
            m_code = new SourceCode();
            
            m_types = new NodeTypes();
            
            //JsonIO t = new JsonIO();
            //t.Test();
	
        }

        public void doUpdate() {
            this.invalidate();
            this.repaint();
        }

        public void syncTypes(SimpleNode n) {
            int x = m_types.findTypeAdd(n.getType());
            m_types.setMandatory(x, n.isMandatory());
            for (int i=0; i<m_nodesUsed; i++) {
                if (m_nodes[i].getType().equals(n.getType())) {
                    m_nodes[i].setMandatory(n.isMandatory());
                }
            }
        }

        public void initTypes() {
            m_types.reset();
            for (int i=0; i<m_nodesUsed; i++) {
                m_types.setAddMandatory(m_nodes[i].getType(), m_nodes[i].isMandatory());
            }
        }
        
        void load() {

            JFileChooser chooser2 = new JFileChooser();
            chooser2.setMultiSelectionEnabled(false);
        
            chooser2.setCurrentDirectory(new File(m_settings.getLast()));
              
            //chooser2.setCurrentDirectory(new File(m_dataDir));
            chooser2.setDialogTitle("Write to:");

            int userSelection = chooser2.showOpenDialog(new JFrame());
        
        
            if (userSelection != JFileChooser.APPROVE_OPTION) {
                return;
            }
        
        
            File fileToSave = chooser2.getSelectedFile();
        
            System.out.println("out: " + fileToSave.getAbsolutePath() + "\n");

            loadData(fileToSave.getAbsolutePath());
            initTypes();
            
            m_settings.setLast(fileToSave.getAbsolutePath());
        }
        
        void loadData(String fileName) {

            JsonIO io = new JsonIO();

            io.read(fileName);
            
            
            
            m_nodesUsed = io.populateNodes(m_nodes);
            m_edgesUsed = io.populateEdges(m_edges, m_nodes, m_nodesUsed);
            io.populateTypes(m_nodes, m_nodesUsed);
            

            m_havePath = false;
            String [] best = io.getBestPath();
            if (best != null) {
                m_havePath = true;
                for (int i=1; i<best.length; i++) {
                    int idx = this.findEdge(best[i-1], best[i]);
                    if (idx >= 0) {
                        m_edges[idx].setBest(true);
                    }
                }
            }


        }
              
        
        public void save() {
        
                    
            JFileChooser chooser2 = new JFileChooser();
            chooser2.setMultiSelectionEnabled(false);
        
        
              
            //chooser2.setCurrentDirectory(new File(m_dataDir));
            chooser2.setDialogTitle("Write to:");
            
            if (!m_settings.getLast().equals(""))
                chooser2.setCurrentDirectory(new File(m_settings.getLast()));

            int userSelection = chooser2.showSaveDialog(new JFrame());
        
        
            if (userSelection != JFileChooser.APPROVE_OPTION) {
                return;
            }
        
        
            File fileToSave = chooser2.getSelectedFile();
        
            
            System.out.println("out: " + fileToSave.getAbsolutePath() + "\n");

            JsonIO io = new JsonIO();
            
            io.fill(m_nodes, m_nodesUsed, m_edges, m_edgesUsed);
            
            io.write(fileToSave.getAbsolutePath());
        
            m_settings.setLast(fileToSave.getAbsolutePath());
        }
        
        
        
        public void setParent(MainPanel p) {
            m_parent = p;
        }
              
        public void addNewLevel() {
            m_parent.createAndShowGuiNew("");
        }
        
        
        SourceCode sourceCode() {
            return m_code;
        }
        
        void addNode(int x, int y) {
            m_nodes[m_nodesUsed] = new SimpleNode();
            m_nodes[m_nodesUsed].setCoords(x, y);
            String name = "node";
            name += m_nodesUsed;
            m_nodes[m_nodesUsed].setName(name);

            m_nodesUsed++;
            
                     
        }

        void addEdge(int from, int to) {
            m_edges[m_edgesUsed] = new SimpleEdge();
            String name = "edge";
            name += m_edgesUsed;
            m_edges[m_edgesUsed].setName(name);
            m_edges[m_edgesUsed].setFunction("func_" + name);
            m_edges[m_edgesUsed].Set(from, to);
            
            m_edgesUsed++;
            
                     
        }

        void SetFromNode() {
            m_fromNode = m_selected;
        }
        
        void deleteNode() {
            if (m_selected < 0) {
                deleteEdge();
                return;
            }
        
            m_fromNode = -1;
            
            m_nodes[m_selected] = m_nodes[m_nodesUsed-1];
            
            m_nodesUsed--;
            
            for (int i=0; i<m_edgesUsed; i++) {
                if (m_edges[i].from() == m_selected || m_edges[i].to() == m_selected) {
                    m_edges[i] = m_edges[m_edgesUsed-1];
                    m_edgesUsed--;
                    i--;
                    continue;
                }
                
                m_edges[i].Fix(m_nodesUsed, m_selected);
            }
            
        }

        void deleteEdge() {
            if (m_selectedEdge < 0)
                return;
            
            m_edges[m_selectedEdge] = m_edges[m_edgesUsed-1];
            
            m_edgesUsed--;
                        
        }

        void renew() {
            m_edgesUsed = 0;
            m_nodesUsed = 0;
            m_selected = -1;
            m_selectedEdge = -1;
        }

        
        void SetScroll(JScrollPane p) {
            scroll = p;
        }

        public void SetMouse(PanelListener listener) {
            this.listener = listener;
        }

        private int findNode(String label) {
            for (int i=0; i<m_nodesUsed; i++) {
                if (m_nodes[i].getName().equals(label))
                    return i;
            }
            return -1;
        }

        private int findEdge(String fromNode, String toNode) {
            int from = findNode(fromNode);
            int to = findNode(toNode);
            return findEdge(from, to);
        }

        private int findEdge(int from, int to) {
            for (int i=0; i<m_edgesUsed; i++) {
                if (m_edges[i].from() == from && m_edges[i].to() == to)
                    return i;
            }
            return -1;
        }
        
        private void drawArrowLine(Graphics g, int x1, int y1, int x2, int y2, int d, int h, boolean high) {
            int dx = x2 - x1, dy = y2 - y1;
            double D = Math.sqrt(dx*dx + dy*dy);
            double xm = D - d, xn = xm, ym = h, yn = -h, x;
            double sin = dy / D, cos = dx / D;

            x = xm*cos - ym*sin + x1;
            ym = xm*sin + ym*cos + y1;
            xm = x;

            x = xn*cos - yn*sin + x1;
            yn = xn*sin + yn*cos + y1;
            xn = x;

            int[] xpoints = {x2, (int) xm, (int) xn};
            int[] ypoints = {y2, (int) ym, (int) yn};

            if (m_havePath) {
                if (high) {
                    g.setColor(Color.black);
                } else {
                    g.setColor(new Color(200, 200, 200));
                }
            }
            g.drawLine(x1, y1, x2, y2);
            g.fillPolygon(xpoints, ypoints, 3);
        }    
        
        String checkSelection(int x, int y) {
            int i;
            m_selected = -1;
            String s = new String();
            
            for (i=0; i<m_nodesUsed; i++) {

                m_nodes[i].setSelected(false);
                if (m_nodes[i].isHit(x, y)) {
                    m_selected = i;
                    m_nodes[i].setSelected(true);
                }                 
            }
            
            m_selectedEdge = -1;
            for (i=0; i<m_edgesUsed; i++) {

                m_edges[i].setSelected(false);
                if (m_edges[i].isHit(x, y)) {
                    m_selectedEdge = i;
                    m_edges[i].setSelected(true);
                    s = m_edges[i].getName();
                }                 
            }
 
            if (m_selected == -1)
                m_fromNode = -1;
            if (m_fromNode >= 0 && m_selected >= 0) {
                            
                System.out.println("Adding edge from " + m_fromNode + " to " + m_selected);        
                
                addEdge(m_fromNode, m_selected);
                m_selected = -1;
                m_fromNode = -1;
            }
            
            return s;
        }
        
        void updateSelected(int x, int y) {
            if (m_selected >= 0) {
                m_nodes[m_selected].setCoords(x, y);
            }
        }

        SimpleNode getSelectedNode() {
            if (m_selected == -1)
                return null;
            return m_nodes[m_selected];
        }

        SimpleEdge getSelectedEdge() {
            if (m_selectedEdge == -1)
                return null;
            return m_edges[m_selectedEdge];
        }

        
        public void printPNG() {

                    
            JFileChooser chooser2 = new JFileChooser();
            chooser2.setMultiSelectionEnabled(false);
        
        
              
            //chooser2.setCurrentDirectory(new File(m_dataDir));
            chooser2.setDialogTitle("Write to:");

            int userSelection = chooser2.showSaveDialog(new JFrame());
        
        
            if (userSelection != JFileChooser.APPROVE_OPTION) {
                return;
            }
        
        
            File fileToSave = chooser2.getSelectedFile();
        
            System.out.println("out: " + fileToSave.getAbsolutePath() + "\n");
            String file = fileToSave.getAbsolutePath();
        
            if (!file.contains(".png"))
                file += ".png";

            
            
            // Test printing
            BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            printAll(g);
            g.dispose();
            try { 
                ImageIO.write(image, "png", new File(file)); 
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            int i;
            //System.out.println("Painting");        

            
            Graphics2D g2d = (Graphics2D) g;
            //g2d.drawLine(10, 100, 300, 10);
              
            //g2d.drawRoundRect(60, 60, 50, 20, 25, 10);
                    // width, height at the end
              
            
            for (i=0; i<m_nodesUsed; i++)
                m_nodes[i].paintComponent(g);

            g2d.setStroke(new BasicStroke(1.5f));
            for (i=0; i<m_edgesUsed; i++) {
                int x1 = m_nodes[m_edges[i].from()].x();
                int x2 = m_nodes[m_edges[i].to()].x();
                int y1 = m_nodes[m_edges[i].from()].y();
                int y2 = m_nodes[m_edges[i].to()].y();
            
                int rx1 = m_nodes[m_edges[i].from()].bestX(x2, y2, true);
                int rx2 = m_nodes[m_edges[i].to()].bestX(x1, y1, false);
                int ry1 = m_nodes[m_edges[i].from()].bestY(x2, y2, true);
                int ry2 = m_nodes[m_edges[i].to()].bestY(x1, y1, false);

                if (i == m_selectedEdge)
                    g2d.setStroke(new BasicStroke(3.5f));
                else
                    g2d.setStroke(new BasicStroke(1.5f));
                
                boolean bHigh = false;
                if (m_edges[i].isBest())
                    bHigh = true;
                drawArrowLine(g, rx1, ry1, rx2, ry2, 10, 5, bHigh);
                m_edges[i].setCoords(rx1, ry1, rx2, ry2);
                m_edges[i].drawName(g2d);
            
            }
            
            
            //SimpleNode n = new SimpleNode();
            //n.paintComponent(g);
        }
    
    
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(1200, 900);
        }
    }

    private class PanelListener implements MouseMotionListener, MouseListener {
        
        public PanelListener() {
            //System.out.println("MOUSE Listener!!!\n");        
            
        }
        
        @Override
        public void mouseMoved(MouseEvent e) {
            //System.out.println("MOUSE moved!!!");
        }
    
        @Override
        public void mouseClicked(MouseEvent event) {
                    /* source is the object that got clicked
                     * 
                     * If the source is actually a JPanel, 
                     * then will the object be parsed to JPanel 
                     * since we need the setBackground() method
                     */
                    
            //System.out.println("MOUSE clicked!!!\n");

            Object source = event.getSource();
            /*if(source instanceof JPanel){
                JPanel panelPressed = (JPanel) source;
                panelPressed.doUpdate();
                //panelPressed.setBackground(Color.blue);
            }*/
            if(source instanceof Drawing){
                Drawing panelPressed = (Drawing) source;
                panelPressed.doUpdate();
                //panelPressed.setBackground(Color.blue);
            }
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
            //System.out.println("MOUSE enter!!!");
        }

        @Override
        public void mouseExited(MouseEvent arg0) {
            //System.out.println("MOUSE exit!!!");

        }


        
        
        @Override
        public void mousePressed(MouseEvent arg0) {
            //System.out.println("MOUSE pressed!!!");

            int x = arg0.getX();
            int y = arg0.getY();

            Object source = arg0.getSource();


            
                            

            int b = arg0.getButton();

            //Drawing panelPressed = (Drawing) source;
            
            if (b == 1) {
                //left = panelPressed.SetLeft(left);
                //right = panelPressed.SetRight(right);
            }
            if (b == 2) {
            }
            if (b == 3) {
                
                final JPopupMenu popup = new JPopupMenu();

                // Add menu items
                JMenuItem menuItem = new JMenuItem("Add new node"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "New node");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "New Project clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            panelPressed.addNode(x, y);
                            panelPressed.doUpdate();
                        }
                        

                    }
                });
                
                popup.add(menuItem);

                //------------------------------------------------------------------------
                menuItem = new JMenuItem("Edit node/edge"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "Edit the node or edge");
                menuItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Edit clicked!");
                        SimpleNode theNode = null;
                        SimpleEdge theEdge = null;
                        SourceCode sc = null;
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            theNode = panelPressed.getSelectedNode();
                            theEdge = panelPressed.getSelectedEdge();
                            sc = panelPressed.sourceCode();
                        }

                        if (theNode != null) {
                            // MGG: Update TYPES
                            //updateTypes();
                            Drawing panelPressed = (Drawing) source;
                            NodeDialog nd = new NodeDialog();
                            nd.setNode(theNode, sc, panelPressed);
                            nd.setVisible(true);
                        }

                        if (theEdge != null) {
                            EdgeDialog nd = new EdgeDialog();
                            nd.setEdge(theEdge);
                            nd.setVisible(true);
                        }


                        //if(source instanceof Drawing){
                          //  Drawing panelPressed = (Drawing) source;
                            //panelPressed.doUpdate();
                        //}


                    }
                });
                
                popup.add(menuItem);

                
                //------------------------------------------------------------------------
                menuItem = new JMenuItem("Connect node"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "Connect the node");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Delete clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            panelPressed.SetFromNode();
                            panelPressed.doUpdate();
                        }

                    }
                });
                
                popup.add(menuItem);

                popup.addSeparator();
                
                //------------------------------------------------------------------------
                menuItem = new JMenuItem("Delete node/edge"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "Delete the node");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Delete clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            panelPressed.deleteNode();
                            panelPressed.doUpdate();
                        }

                    }
                });
                
                popup.add(menuItem);

                popup.addSeparator();
                
                               //------------------------------------------------------------------------
                menuItem = new JMenuItem("Expand node"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "Expand the node");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Delete clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            if (panelPressed.getSelectedNode() != null) {
                                panelPressed.addNewLevel();
                                panelPressed.doUpdate();
                            }
                        }

                    }
                });
                
                popup.add(menuItem);

                popup.addSeparator();

                
                
                
                
                
                
                //------------------------------------------------------------------------
                menuItem = new JMenuItem("Load"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "Load");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Delete clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            panelPressed.load();
                            panelPressed.doUpdate();
                        }

                    }
                });
                
                popup.add(menuItem);

                //------------------------------------------------------------------------
                menuItem = new JMenuItem("Save"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "Save");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Delete clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            panelPressed.save();
                            panelPressed.doUpdate();
                        }

                    }
                });
                
                popup.add(menuItem);

                
                //------------------------------------------------------------------------
                menuItem = new JMenuItem("New"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "New");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Delete clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            panelPressed.renew();
                            panelPressed.doUpdate();
                        }

                    }
                });
                
                popup.add(menuItem);

                
                                
                popup.addSeparator();

                
                menuItem = new JMenuItem("Print as PNG"); 
                        
                menuItem.setMnemonic(KeyEvent.VK_P);
                menuItem.getAccessibleContext().setAccessibleDescription(
                "Print to PNG file");
                menuItem.addActionListener(new ActionListener() {
 
                    public void actionPerformed(ActionEvent e) {
                        //JOptionPane.showMessageDialog(frame, "Delete clicked!");
                        if(source instanceof Drawing){
                            Drawing panelPressed = (Drawing) source;
                            panelPressed.printPNG();
                            panelPressed.doUpdate();
                        }

                    }
                });
                
                popup.add(menuItem);
               
                
                
                //============================ Done menu items ===========================
                
                
                popup.show(arg0.getComponent(),
                           arg0.getX(), arg0.getY());

            } 

            if (source instanceof Drawing){
                Drawing panelPressed = (Drawing) source;
                String s = panelPressed.checkSelection(x, y);
                if (s != "") {
                
                }
                panelPressed.doUpdate();

            }
                
            //System.out.println("MOUSE pressed, x=" + x + " y=" + y + " button: " + b + "\n");


            
            
            
        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
            //System.out.println("MOUSE released!!!");

        }

        @Override
        public void mouseDragged(MouseEvent me) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            int x = me.getX();
            int y = me.getY();

            //System.out.println("MOUSE dragged: " + x + "," + y);

                        
            Object source = me.getSource();


            if(source instanceof Drawing){
                Drawing panelPressed = (Drawing) source;
                panelPressed.updateSelected(x, y);
                panelPressed.doUpdate();

            }
        }
        


    }


}
