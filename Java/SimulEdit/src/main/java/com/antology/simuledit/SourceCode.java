/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

import javax.swing.JTextArea;

/**
 *
 * @author manfred
 */
public class SourceCode {

    Function [] m_func;
    int m_count = 0;
    
    SourceCode() {
        m_func = new Function[64];
        
        addFunction("do_nothing");
        m_func[0].addLine("  -- add source code here");
        /*m_func[0].addLine("");
        m_func[0].addLine("    -- this function is a dummy and does nothing");
        m_func[0].addLine("");
        m_func[0].addLine("return source, relation, target, directive");
        m_func[0].addLine("");
        m_func[0].addLine("end");*/
    }
    
    void addFunction(String name) {
        if (name != "") {
            m_func[m_count] = new Function();
            m_func[m_count].setName(name);
            m_count++;
        }
    }
    
    void clear() {
        m_count = 0;
    }
    
    int count() {
        return m_count;
    }
    
    String name(int i) {
        return m_func[i].name();
    }
    
    
    int index(String func) {
        int i;
        int idx = -1;
        for (i=0; i<m_count; i++) {
            if (m_func[i].name() == func)
                idx = i;
        }
        if (idx == -1)
            addFunction(func);
        idx = m_count - 1;
        return idx;
    }
    
    void update(JTextArea a, String func) {
        if (func == "")
            return;
        int idx = index(func);
        
        update(a, idx);
    }
    
    void update(JTextArea a, int idx) {
        String s = new String();
        
        s = m_func[idx].asString();
        a.setText(s);
    }
    
    
    
    void read(String name) {
    
    }
    
    void write(String name) {
    
    }
    
}
