/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

/**
 *
 * @author manfred
 */
public class EdgeCode {
    private Function m_func;
    
    public Function getFunc() {
        return m_func;
    }
    
    public EdgeCode() {
        m_func = new Function();
    }
    
    void setName(String name) {
        m_func.setName(name);
    }
    
    public String getText() {
        return m_func.asString();
    }
    
    public void clear() {
        m_func.clear();
    }
    
    public void addLine(String l) {
        m_func.addLine(l);
    }
    
    public void setText(String l) {
        m_func.clear();
        m_func.addLine(l);
    }
    
}
