/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.antology.simuledit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author manfred
 */
public class Settings {
    private String m_last;
    private String m_fileName;
    
    public Settings() {
        m_last = new String();
        m_fileName = "simuledidit.cfg";
    }
    
    public String getLast() {
        return m_last;
    }

    public void setLast(String last) {
        m_last = last;
        write();
    }
    
    public void write() {
         
        try {
            
            File file = new File(m_fileName);
            if (file.exists()) {
                file.delete();                
            }
            FileWriter fw = new FileWriter(m_fileName);
            fw.write("last\t" + m_last + "\n");  
            fw.close();
        } catch (Exception e) {
            System.out.println("Could not save settings");
        }
   
    }
    
    public void read() {
            
        File file = new File(m_fileName); 
        try {
            Scanner sc = new Scanner(file); 
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] split = line.split("\\s+");
                if (split.length < 2)
                    continue;
                if (split[0].equals("last"))
                    m_last = split[1];
            }
        } catch (Exception e) {
            System.out.println("WARNING: Config not found.");
        }

    }
}
