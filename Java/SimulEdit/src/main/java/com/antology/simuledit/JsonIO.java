/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;


import java.io.FileNotFoundException;
import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.JSONParser;


/**
 *
 * @author manfred
 */
public class JsonIO {


    private JSONObject m_json;
    private JSONObject m_meta;

    JsonIO() {
    
    }
    
    void write(String fileName) {
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(m_json.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    
    
    }


    void read(String fileName) {
        
               
        JSONParser jsonParser = new JSONParser();
         
        try (FileReader reader = new FileReader(fileName))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
 
            m_json = (JSONObject) obj;
            
            
            m_meta = (JSONObject)m_json.get("meta");

            
            //System.out.println(employeeList);
             
            //Iterate over employee array
            //employeeList.forEach( emp -> parseEmployeeObject( (JSONObject) emp ) );
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(JsonIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }


    
    public void add(String node, JsonIO json) {
        Object n = m_json.get("nodes");
        JSONObject o = (JSONObject)n;
        
        Object nn = m_json.get(node);
        JSONObject oo = (JSONObject)nn;
        
        oo.put("subtree", oo);
    }
    
    int populateNodes(SimpleNode [] nodes) {

        Object n = m_json.get("nodes");
        JSONArray o = (JSONArray)n;

        int i, j;
        
        int num = o.size();
        
        for (i=0; i<o.size(); i++) {
            JSONObject one = (JSONObject)o.get(i);

            if (nodes[i] == null)
                nodes[i] = new SimpleNode();
            
            nodes[i].setName((String)one.get("name"));
            nodes[i].setType((String)one.get("type"));
            nodes[i].setFunction((String)one.get("function"));

            if (one.containsKey("code")) {
                HTML2Text ht = new HTML2Text();
                nodes[i].getCode().setText(ht.toText((String)one.get("code")));
            }

            //if (one.containsKey("optional"))
              //  nodes[i].setOptional((String)one.get("optional"));
            int x = (int)(long)one.get("x");
            int y = (int)(long)one.get("y");
            nodes[i].setCoords(x, y); 

            AttributeList a = nodes[i].getAttributes();
            
            JSONArray attr = (JSONArray)one.get("attributes");
 
            int ss = attr.size();
            a.setSize(attr.size());
            for (j=0; j<attr.size(); j++) {
                JSONObject s = (JSONObject)attr.get(j);
                for (Object key : s.keySet())
                {
                    if (s.get(key) != null) {
                        
                        Object oo = s.get(key);

                                                
                        String type = "string";
                        String value = new String();
                        String id = (String)key;
                        
                        if (id.contains("_comment"))
                            continue;
                        
                        if (oo instanceof String) {
                            value = (String)oo;
                        } else {
                            if (oo instanceof Integer || oo instanceof Long) {
                                type = "int";         
                                long v = (long)oo;
                                value += v;
                            } else {
                                if (oo instanceof Double) {
                                    type = "double";  
                                    double v = (double)oo;
                                    value += v;
                                }
                            }
                            
                        }
                       
                        a.set(j, id, value, type, "");
                       
                        
                    }
                }   
            }   
        }
        
        
        return num;
    }
    
    int index(SimpleNode [] nodes, int len, String name) {
        for (int i=0; i<len; i++) {
            if (nodes[i].getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public String [] getBestPath() {
        if (!m_json.containsKey("best_path"))
            return null;
        Object n = m_json.get("best_path");
        JSONArray o = (JSONArray)n;
        int i;
        
        int num = o.size();
        
        String [] out = new String[num];
        
        for (i=0; i<o.size(); i++) {
            JSONObject one = (JSONObject)o.get(i);
            String name = (String)one.get("name");
            out[i] = name;
        }
        return out;
    }
    
    void populateTypes(SimpleNode [] nodes, int nodelen) {
        if (!m_json.containsKey("types"))
            return;
        
        Object n = m_json.get("types");
        JSONArray o = (JSONArray)n;
        int i, j;
        
        int num = o.size();
        
        for (i=0; i<o.size(); i++) {
            JSONObject one = (JSONObject)o.get(i);
            String name = (String)one.get("name");
            String prop = (String)one.get("property");
            for (j=0; j<nodelen; j++) {
                if (nodes[i] != null && nodes[i].getType().equals(name)) {
                    if (prop.equals("mandatory"))
                        nodes[i].setMandatory(true);
                    else
                        nodes[i].setMandatory(false);
                }
            }
        }
    }
    
    int populateEdges(SimpleEdge [] edges, SimpleNode [] nodes, int nodelen) {
        Object n = m_json.get("edges");
        JSONArray o = (JSONArray)n;

        int i, j;
        
        int num = o.size();
        
        for (i=0; i<o.size(); i++) {
            JSONObject one = (JSONObject)o.get(i);

            if (edges[i] == null)
                edges[i] = new SimpleEdge();
            
            edges[i].setName((String)one.get("name"));
            edges[i].setType((String)one.get("type"));
            edges[i].setFunction((String)one.get("function"));
            
            if (one.containsKey("code")) {
                HTML2Text ht = new HTML2Text();
                edges[i].getCode().setText(ht.toText((String)one.get("code")));
            }
            
            int x1 = (int)(long)one.get("x1");
            int y1 = (int)(long)one.get("y1");
            int x2 = (int)(long)one.get("x2");
            int y2 = (int)(long)one.get("y2");
            edges[i].setCoords(x1, y1, x2, y2);
            int from = index(nodes, nodelen, (String)one.get("from"));
            int to = index(nodes, nodelen, (String)one.get("to"));
            edges[i].setFromTo(from, to);                  

            
        
            AttributeList a = edges[i].getAttributes();
            
            JSONArray attr = (JSONArray)one.get("attributes");
 
            a.setSize(attr.size());
            for (j=0; j<attr.size(); j++) {
                JSONObject s = (JSONObject)attr.get(j);
                for (Object key : s.keySet())
                {
                    if (s.get(key) != null) {
                        
                        Object oo = s.get(key);

                                                
                        String type = "string";
                        String value = new String();
                        String id = (String)key;
                        
                        if (id.contains("_comment"))
                            continue;
                        
                        if (oo instanceof String) {
                            value = (String)oo;
                        } else {
                            if (oo instanceof Integer || oo instanceof Long) {
                                type = "int";         
                                long v = (long)oo;
                                value += v;
                            } else {
                                if (oo instanceof Double) {
                                    type = "double";  
                                    double v = (double)oo;
                                    value += v;
                                }
                            }
                            
                        }
                       
                        a.set(j, id, value, type, "");
                       
                        
                    }
                }   
            }   
        }
 
        return num;
    }
    
    
    void fill(SimpleNode [] nodes, int nodeCount,
              SimpleEdge [] edges, int edgeCount) {
        m_json = new JSONObject();
        
        
        
        
        JSONObject meta = new JSONObject();
        //JSONObject n = new JSONObject();
        //JSONObject e = new JSONObject();

        JSONArray n = new JSONArray();
        JSONArray e = new JSONArray();
        
        meta.put("name", "test");
        meta.put("version", "0.0.1");
        meta.put("revision", 1);
        
        m_json.put("meta", meta);
        

        int i, j;
       

        //JSONObject types = new JSONObject();

        // TYPES
        JSONArray tt = new JSONArray();
                

        String [] alltypes = new String[nodeCount];
        int nTypes = 0;
        for (i=0; i<nodeCount; i++) {
            
            if (nodes[i].getType().equals(""))
                continue;
            boolean found = false;
            for (j=0; j<nTypes; j++) {
                if (nodes[i].getType().equals(alltypes[j]))
                    found = true;
                
            }
            if (found)
                continue;
            
            alltypes[nTypes] = nodes[i].getType();
            nTypes++;
            
            JSONObject oneType = new JSONObject();
            oneType.put("name", nodes[i].getType());
            if (nodes[i].isMandatory())
                oneType.put("property", "mandatory");
            else
                oneType.put("property", "optional");
            tt.add(oneType);
        }
        //types.put(tt);
        m_json.put("types", tt);
        
        
        for (i=0; i<nodeCount; i++) {
                    
            JSONObject one = new JSONObject();
            one.put("name", nodes[i].getName());
            one.put("type", nodes[i].getType());
            one.put("function", nodes[i].function());
            //if (nodes[i].isOptional()) {
            //    one.put("optional", "true");
            //}

            HTML2Text ht = new HTML2Text();
            one.put("code", ht.toHTML(nodes[i].getCode().getText()));

            one.put("x", nodes[i].x());
            one.put("y", nodes[i].y());
            
            JSONArray attr = new JSONArray();
            
            for (j=0; j<nodes[i].getAttributes().count(); j++) {

                JSONObject xy = new JSONObject();

                if (nodes[i].getAttributes().get(j).type.equals("string")) {
                    xy.put(nodes[i].getAttributes().get(j).name, nodes[i].getAttributes().get(j).value);
                } else {
                    if (nodes[i].getAttributes().get(j).type.equals("int"))
                        xy.put(nodes[i].getAttributes().get(j).name, Integer.parseInt(nodes[i].getAttributes().get(j).value));
                    else
                        xy.put(nodes[i].getAttributes().get(j).name, Double.parseDouble(nodes[i].getAttributes().get(j).value));

                }
                xy.put(nodes[i].getAttributes().get(j).name + "_comment", nodes[i].getAttributes().get(j).comment);        
                attr.add(xy);
            }
            one.put("attributes", attr);
            n.add(one);
        }
        
        m_json.put("nodes", n);
        
        for (i=0; i<edgeCount; i++) {
                    
            JSONObject one = new JSONObject();
            one.put("name", edges[i].getName());
            one.put("type", edges[i].getType());
            one.put("function", edges[i].function());
            

            one.put("from", nodes[edges[i].from()].getName());
            one.put("to", nodes[edges[i].to()].getName());
            
            one.put("x1", edges[i].x1());
            one.put("y1", edges[i].y1());
            one.put("x2", edges[i].x2());
            one.put("y2", edges[i].y2());
                
            HTML2Text ht = new HTML2Text();
            one.put("code", ht.toHTML(edges[i].getCode().getText()));
                    

                     
            JSONArray attr = new JSONArray();
   
            for (j=0; j<edges[i].getAttributes().count(); j++) {

                JSONObject xy = new JSONObject();

                if (edges[i].getAttributes().get(j).type.equals("string")) {
                    xy.put(edges[i].getAttributes().get(j).name, edges[i].getAttributes().get(j).value);
                } else {
                    if (edges[i].getAttributes().get(j).type.equals("int"))
                        xy.put(edges[i].getAttributes().get(j).name, Integer.parseInt(edges[i].getAttributes().get(j).value));
                    else
                        xy.put(edges[i].getAttributes().get(j).name, Double.parseDouble(edges[i].getAttributes().get(j).value));

                }
                xy.put(edges[i].getAttributes().get(j).name + "_comment", edges[i].getAttributes().get(j).comment);        
            
                attr.add(xy);
            }
            one.put("attributes", attr);
            e.add(one);
        }
        
        m_json.put("edges", e);
        
        
    }
    
    
    public void Test() {
    
        JSONObject obj = new JSONObject();
        obj.put("name", "mkyong.com");
        obj.put("age", 100);

        JSONArray list = new JSONArray();
        list.add("msg 1");
        list.add("msg 2");
        list.add("msg 3");
        

        obj.put("messages", list);

        try (FileWriter file = new FileWriter("test.json")) {
            file.write(obj.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(obj);
    
    }
    
    
}
