/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.RenderingHints;

/**
 *
 * @author manfred
 */
public class SimpleNode {
    
    private int m_x;
    private int m_y;
    private int m_w;
    private int m_h;
    private int m_offX;
    private int m_offY;
    private boolean m_sel;
    private String m_name;
    private String m_type;
    AttributeList m_attr;
    private boolean m_mandatory;
    private boolean m_optional;
    private EdgeCode m_code;
    
    String m_function;
    
    
    SimpleNode() {
        m_code = new EdgeCode();
        
        m_mandatory = true;
        
        m_x = 150;
        m_y = 150;
        m_w = 130;
        m_h = 40;
    
        m_offX = 0;
        m_offY = 0;
        m_sel = false;
        
        m_attr = new AttributeList();
        
        Attribute a = new Attribute();
        a.name = "cost";
        a.value = "0";
        a.type = "float";
        a.comment = "node cost";
        
        Attribute b = new Attribute();
        b.name = "duration";
        b.value = "1";
        b.type = "int";
        b.comment = "node duration";


        Attribute c = new Attribute();
        c.name = "risk";
        c.value = "0.0";
        c.type = "float";
        c.comment = "node risk";

        m_attr.add(a);
        m_attr.add(b);
        m_attr.add(c);
        m_function = new String();
        m_function = "none";
        m_type = new String();
    }
    
    public EdgeCode getCode() {
        return m_code;
    }
    
    public boolean isMandatory() {
        return m_mandatory;
    }

    public boolean isOptional() {
        return !m_mandatory;
    }

    public void setMandatory(boolean b) {
        m_mandatory = b;
    }

    public void setOptional(String s) {
        if (s.equals("true"))
            m_mandatory = false;
        if (s.equals("1"))
            m_mandatory = false;
    }
    
    public AttributeList getAttributes() {
        return m_attr;
    }
    
    String function() {
        return m_function;
    }
    
    void setFunction(String f) {
        m_function = f;
    }
    
    public int x() {return m_x;}
    public int y() {return m_y;}
    
    int abs(int a) {
        if (a < 0)
            return -a;
        return a;
    }
    
    public int bestX(int x, int y, boolean to) {
        int slack = m_w/4;
        if (to)
            slack = -slack;
        if (abs(x-m_x) > abs(y-m_y)) {
            if (x > m_x)
                return m_x + m_w;
            else
                return m_x;
        } else {
            return m_x + m_w/2 + slack;
        }
    } 

    public int bestY(int x, int y, boolean to) {
        int slack = m_h/4;
        if (!to)
            slack = -slack;
        if (abs(x-m_x) > abs(y-m_y)) {
            return m_y + m_h/2 + slack;
        } else {
            if (y > m_y)
                return m_y + m_h;
            else
                return m_y;
        }
    } 
    
    public String getName() {
        return m_name;
    }
    
    public void setName(String name) {
        m_name = name;
    }

    public String getType() {
        return m_type;
    }
    
    public void setType(String name) {
        m_type = name;
    }
    
    public void setCoords(int x, int y) {
        m_x = x-m_offX;
        m_y = y-m_offY;
    }
    
    public boolean isHit(int x, int y) {
        if (x >= m_x && x <= m_x+m_w && y > m_y && y < m_y + m_h) {
            m_offX = x-m_x;
            m_offY = y-m_y;
            return true;
        } else {
            return false;
        }
    }
    
    public void setSelected(boolean b) {
        m_sel = b;
    }
    
    public void paintComponent(Graphics g) {
                    
        Graphics2D g2d = (Graphics2D) g;
        
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                             RenderingHints.VALUE_ANTIALIAS_ON);

        if (m_sel)
            g2d.setStroke(new BasicStroke(4f));
        else
            g2d.setStroke(new BasicStroke(2f));
            
        if (m_type != null) {
            if (m_type.equals("cloud"))        
                g2d.setColor(new Color(223, 0, 45));

            if (m_type.equals("customer"))        
                g2d.setColor(new Color(0, 70, 212));

            if (m_type.equals("data"))        
                g2d.setColor(new Color(0, 112, 33));

            if (m_type.equals("information"))        
                g2d.setColor(new Color(0, 112, 145));

            if (m_type.equals("software"))        
                g2d.setColor(new Color(99, 99, 0));

        }
        
        
        g2d.drawRoundRect(m_x, m_y, m_w, m_h, 25, 10);


        g2d.setColor(Color.BLACK);
                            
                       
        
        g2d.drawString(m_name, m_x+12, m_y+20); 

    }
    
    
}
