/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

/**
 *
 * @author manfred
 */
public class HTML2Text {
    
    HTML2Text() {
    
    }
    
    public String toText(String a) {
        String out = new String();
        for (int i=0; i<a.length()-4; i++) {
            if (a.charAt(i) == '<'
                && a.charAt(i+1) == 'b'
                && a.charAt(i+2) == 'r'
                && a.charAt(i+3) == '>') {
                out += '\n';
                i += 3;
            } else {
                out += a.charAt(i);
            }
        }
        
        return out;
    }

    public String toHTML(String a) {
        String out = new String();
        
        for (int i=0; i<a.length(); i++) {
            if (a.charAt(i) == '\n') {
                out += "<br>";
            } else {
                out += a.charAt(i);
            }
        }
        return out;
    }
    

}
