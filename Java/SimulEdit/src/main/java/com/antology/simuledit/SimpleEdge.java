/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 *
 * @author manfred
 */
public class SimpleEdge {

    private boolean m_sel = false;
    int m_from = -1;
    int m_to = -1;
    String m_name;
    
    int m_x1 = -1;
    int m_y1 = -1;
    
    int m_x2 = -1;
    int m_y2 = -1;
    
    
    
    private String m_type;
    AttributeList m_attr;
    
    String m_function;

    EdgeCode m_code;
    
    private boolean m_best = false;
    
    SimpleEdge() {
            
        m_attr = new AttributeList();
        m_function = new String();
        m_type = new String();
        
        m_function = "none";
        
        m_code = new EdgeCode();
        
        Attribute a = new Attribute();
        a.name = "cost";
        a.value = "0";
        a.type = "float";
        a.comment = "edge cost";
        
        Attribute b = new Attribute();
        b.name = "duration";
        b.value = "1";
        b.type = "int";
        b.comment = "edge duration";


        Attribute c = new Attribute();
        c.name = "risk";
        c.value = "0.0";
        c.type = "float";
        c.comment = "edge risk";

        m_attr.add(a);
        m_attr.add(b);
        m_attr.add(c);

    }
    
    public boolean isBest() {
        return m_best;
    }
    public void setBest(boolean b) {
        m_best = b;
    }
    
    public EdgeCode getCode() {
        return m_code;
    }
    
    public int x1() {
        return m_x1;
    }
    public int y1() {
        return m_y1;
    }
    public int x2() {
        return m_x2;
    }
    public int y2() {
        return m_y2;
    }
    
    public void setFromTo(int from, int to) {
        m_from = from;
        m_to = to;
    }
    
    public void drawName(Graphics2D g2d) {
         g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                             RenderingHints.VALUE_ANTIALIAS_ON);

         
        int x = (m_x1 + m_x2)/2;
        int y = (m_y1 + m_y2)/2;
         
        if (m_x1 < m_x2) {
            x += 5;
            y += 5;
        } 
        
        g2d.drawString(m_name, x, y); 
  
    
    }
        
    public AttributeList getAttributes() {
        return m_attr;
    }
    
    String function() {
        return m_function;
    }
    
    void setFunction(String f) {
        m_function = f;
    }

    
    
    
    public void Set(int from, int to) {
        m_from = from;
        m_to = to;
    }
    
    public int from() {
        return m_from;
    }
    public int to() {
        return m_to;
    }
    
   
    public String getName() {
        return m_name;
    }
    
    public void setName(String name) {
        m_name = name;
    }

    public String getType() {
        return m_type;
    }
    
    public void setType(String name) {
        m_type = name;
    }

    
    
    public void setSelected(boolean b) {
        m_sel = b;
    }
    
    public void Fix(int oldIdx, int newIdx) {
        if (m_from == oldIdx)
            m_from = newIdx;
        if (m_to == oldIdx)
            m_to = newIdx;
        
    
    }
    
    public void setCoords(int x1, int y1, int x2, int y2) {
        m_x1 = x1;
        m_y1 = y1;
        m_x2 = x2;
        m_y2 = y2;
    }
    
    //===========================================================
    // For Louie
    private int abs(int x) {
        if (x < 0)
            return -x;
        else
            return x;
    }

    
    
    
    public boolean isHit(int x, int y) {
        int lowX = m_x1;
        if (m_x2 < m_x1)
            lowX = m_x2;

        int lowY = m_y1;
        if (m_y2 < m_y1)
            lowY = m_y2;

        int hiX = m_x1;
        if (m_x2 > m_x1)
            hiX = m_x2;

        int hiY = m_y1;
        if (m_y2 > m_y1)
            hiY = m_y2;
        
        if (x < lowX || x > hiX || y < lowY || y > hiY)
            return false;
        
        double slack = 4;
        
        if (m_x1 == m_x2) {
            
            double diff = (double)x - (double)m_x1;
            if (diff < 0.)
                diff = -diff;
            if (diff < slack)
                return true;
            else
                return false;
        }
        
        double a = (double)(m_y1 - m_y2)/(double)(m_x1 - m_x2);
        double b = (double)m_y1 - a*(double)m_x1;
        
        double yy = a*(double)x + b;
        double xx = ((double)y-b)/a;
        
        double dd = yy - (double)y;
        if (dd < 0.)
            dd = -dd;
        if (dd < slack)
            return true;

        dd = xx - (double)x;
        if (dd < 0.)
            dd = -dd;
        if (dd < slack)
            return true;

        
        return false;
    }

}
