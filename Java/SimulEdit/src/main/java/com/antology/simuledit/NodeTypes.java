/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.antology.simuledit;

/**
 *
 * @author manfred
 */
public class NodeTypes {
    private boolean [] m_mandatory;
    private String [] m_type;
    private int m_count;
    
    public NodeTypes() {
        m_count = 0;
        m_mandatory = new boolean[512];
        m_type = new String[512];
    }
    
    public void reset() {
        m_count = 0;
    }
    
    public int size() {
        return m_count;
    }
    
    public boolean isMandatory(int i) {
        return m_mandatory[i];
    }
    public String getType(int i) {
        return m_type[i];
    }
    public int findType(String typeName) {
        for (int i=0; i<m_count; i++) {
            if (m_type[i].equals(typeName))
                return i;    
        }
        return -1;
    }
    public int findTypeAdd(String typeName) {
        int x = findType(typeName);
        if (x < 0) {
            m_type[m_count] = typeName;
            m_mandatory[m_count] = true;
            m_count++;
            return m_count-1;
        }
        return x;
    }
    
    public void setMandatory(int i, boolean b) {
        m_mandatory[i] = b;
    }
    
    public void setAddMandatory(String typeName, boolean b) {
        int x = findTypeAdd(typeName);
        setMandatory(x, b);
    }
}
