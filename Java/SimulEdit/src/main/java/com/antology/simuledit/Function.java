/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.antology.simuledit;

/**
 *
 * @author manfred
 */
public class Function {
    String m_name;

    String [] m_lines;
    String [] m_header;
    String [] m_footer;
    int m_count = 0;
    
    Function() {
        m_lines = new String[256];

        m_header = new String[8];
        m_footer = new String[7];
        
        m_header[0] = "function test ( source, relation, target)";
        m_header[1] = "";
        m_header[2] = "local json = require('cjson')";

        m_header[3] = "";
        
        m_header[4] = "local s = json.encode (source)";
        m_header[5] = "local r = json.encode (relation)";
        m_header[6] = "local t = json.encode (target)";
        
        m_header[7] = "directive = \"\"";

                
        m_footer[0] = "source = json.decode(s)";
        m_footer[1] = "relation = json.decode(r)";
        m_footer[2] = "target = json.decode(t)";

        m_footer[3] = "";

        m_footer[4] = "return source, relation, target, directive";
        m_footer[5] = "";
        m_footer[6] ="end";


    }
    
    void clear() {
        m_count = 0;
    }
    
    String name() {
        return m_name;
    }
    
    void setName(String name) {
        m_name = name;
    }
    
    void addLine(String s) {
                
        m_lines[m_count] = new String();
        m_lines[m_count] = s;
        m_count++;
    }
    
    String asString() {
        String s = new String();
        //for (int i=0; i<m_header.length; i++) {
          //  s += m_header[i] + "\n";
        //}
        
        for (int i=0; i<m_count; i++) {
            s += m_lines[i] + "\n";
        }
        //for (int i=0; i<m_footer.length; i++) {
        //    s += m_footer[i] + "\n";
       // }
    
        return s;
    }
}
