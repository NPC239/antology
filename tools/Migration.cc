#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsManip.h"
#include "plugins/ManipLua.h"

const int NODES = 8;

int Find(const string & name, const svec<string> & all)
{
  for (int i=0; i<all.isize(); i++) {
    if (all[i] == name)
      return i;
  }

  cout << "NOT FOUND: " << name << endl;
  return -1;
}

int main( int argc, char** argv )
{

  commandArg<string> fileCmmd("-i","input file");
  commandArg<string> luaCmmd("-lua","lua script", "");
  commandLineParser P(argc,argv);
  P.SetDescription("Testing the file parser.");
  P.registerArg(fileCmmd);
  P.registerArg(luaCmmd);
 
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);
  string luaName = P.GetStringValueFor(luaCmmd);
  
  int i, j;

 
  FlatFileParser parser;
  
  parser.Open(fileName);

  svec<string> names;

  AntsGraph ants;

  int all_type = ants.Types().AddType(true);
  int end_type = ants.Types().AddType(false);


  /*
  ANode a1, a2, a3;
  a1.Type() = all_type;
  a1.Label() = "first";
  a1.Cost() = RandomFloat(1.);
  a1.AddEdge(1, 1, RandomFloat(1), 0.1); //index, duration, cost, risk

  ants.push_back(a1, true);
		 

  a2.Type() = all_type;
  a2.Label() = "second";
  a2.Cost() = RandomFloat(1.);
  a2.AddEdge(2, 1, RandomFloat(1), 0.1); //index, duration, cost, risk

  ants.push_back(a2, false);

  a3.Type() = end_type;
  a3.Label() = "last";
  a3.Cost() = RandomFloat(1.);

  ants.push_back(a3, false);
  */

  
  int k = 0;
  while (parser.ParseLine()) {
    if (parser.GetItemCount() == 0)
      continue;
    if (strstr(parser.Line().c_str(), ";") != NULL) {
      for (i=0; i<parser.GetItemCount(); i++) {
	StringParser p;
	p.SetLine(parser.AsString(i), "[");
	names.push_back(p.AsString(0));
	ANode a;
	a.Label() = parser.AsString(i);
	a.Cost() = RandomFloat(1.);
	string t = p.AsString(0);
	
	if (t == "O")
	  a.Type() = end_type;
	
	if (t == "F" || t == "F1" || t == "F2") {
	  a.Type() = end_type;
	  a.Cost() = 222;
	  cout << "END type" << endl;
	} else {
	  a.Type() = all_type;
	  //cout << " type" << endl;
	}
	
	cout << "Adding node " << a.Label() << endl;
	// HARD CODED!!
	if (k < 2)
	  ants.push_back(a, true);
	else
	  ants.push_back(a, false);	  
	k++;
      }

    }

    if (strstr(parser.Line().c_str(), "->") != NULL && strstr(parser.Line().c_str(), "Node") == NULL) {
      int from = Find(parser.AsString(0), names);
      int to = Find(parser.AsString(parser.GetItemCount()-1), names);
      ANode & a = ants[from];
      a.AddEdge(to, 1, RandomFloat(1), 0.1); //index, duration, cost, risk
      cout << "Add edge " << from << " to " << to << endl;
    }
  }

 
  
 
  LuaManip manip;

  if (luaName != "") {
    manip.Open(luaName);
  
    for (i=0; i<ants.isize(); i++)
      ants[i].SetManip(&manip);
  }

   //ants.SetupFullRandom(NODES);
  ants.SetOptimizeCost(true);

  ants.SetMaxRisk(0.8);

  
  RandomFloat(2);
  
  double t = 9999999;

  int v = Find("L", names);
  for (i=0; i<1000; i++) {
    cout << "Round " << i << endl;
    cout << "Cost for validation: " << ants[v].Cost() << " " << v << endl;
    double s = ants.Run(1);
    cout << "SCORE " << s << endl;
    if (s < t) {
      t = s;
      cout << "********* Minimum: " << t << endl;
      ants.Print();
    }
  }
  
  return 0;
}
