#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsJson.h"
#include "plugins/ManipLua.h"


int main( int argc, char** argv )
{

  commandArg<string> fileCmmd("-i","input graph file, text or json");
  commandArg<string> outCmmd("-o","output graph file, text or json", "out.json");
  commandArg<string> luaCmmd("-lua","lua script", "");
  commandArg<int> aCmmd("-ants", "number of cocurrent ants", 1);
  commandArg<int> nCmmd("-n", "iterations", 25);
  commandArg<double> maxTCmmd("-maxtime", "maximum allowed time", -1.);
  commandArg<double> maxCCmmd("-maxcost", "maximum allowed cost", -1.);
  commandArg<double> maxRCmmd("-maxrisk", "maximum allowed risk", -1.);
  commandArg<int> optCmmd("-by", "what to optimize by: 0 = time, 1 = cost, 2 = risk", 0);
  commandLineParser P(argc,argv);
  P.SetDescription("ANTology main executable.");
  P.registerArg(fileCmmd);
  P.registerArg(outCmmd);
  P.registerArg(luaCmmd);
  P.registerArg(aCmmd);
  P.registerArg(nCmmd);
  P.registerArg(maxTCmmd);
  P.registerArg(maxCCmmd);
  P.registerArg(maxRCmmd);
  P.registerArg(optCmmd);
 
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);
  string outName = P.GetStringValueFor(outCmmd);
  string luaName = P.GetStringValueFor(luaCmmd);
  int num_ants = P.GetIntValueFor(aCmmd);
  int N = P.GetIntValueFor(nCmmd);
  double maxTime = P.GetDoubleValueFor(maxTCmmd);
  double maxCost = P.GetDoubleValueFor(maxCCmmd);
  double maxRisk = P.GetDoubleValueFor(maxRCmmd);
  int by = P.GetIntValueFor(optCmmd);

 

  int i, j;

  AntsGraph ants;


  AntsJson js;
  if (strstr(fileName.c_str(), ".json") != NULL) {
    cout << "Reading json format: " << fileName << endl;
    js.ReadFile(ants, fileName);
    ants.Print();
  } else {
    cout << "Reading file format: " << fileName << endl;
    ants.Read(fileName);
  }

  if (by == 1)
    ants.SetOptimizeCost(true);
  
  if (by == 2)
    ants.SetOptimizeRisk(true);
 
  
  ants.SetMaxTime(maxTime);
  ants.SetMaxCost(maxRisk);
  ants.SetMaxRisk(maxCost);
  

  
  //ants.SetupFullRandom(4);

  
  LuaManip manip;

  if (luaName != "") {
    manip.Open(luaName);  
    for (i=0; i<ants.isize(); i++)
      ants[i].SetManip(&manip);
  }


  
  double t = 9999999;

  for (i=0; i<N; i++) {
    cout << "Round " << i << endl;
    double s = ants.Run(num_ants);
    if (s < t) {
      t = s;
      cout << "********* Minimum: " << t << endl;
      ants.Print();
      if (strstr(fileName.c_str(), ".json") != NULL) {
	cout << "Writing json format: " << outName << endl;
	js.WriteFile(ants, outName);
      }

    }
  }
  
  return 0;
}
