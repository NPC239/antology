#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsManip.h"
#include "plugins/ManipLua.h"



int main( int argc, char** argv )
{

  /*commandArg<string> fileCmmd("-i","input file");
  commandArg<int> bCmmd("-from","from column");
  commandArg<int> eCmmd("-to","to column");
  commandArg<bool> nCmmd("-newline","add newline", false);
  commandLineParser P(argc,argv);
  P.SetDescription("Testing the file parser.");
  P.registerArg(fileCmmd);
  P.registerArg(bCmmd);
  P.registerArg(eCmmd);
  P.registerArg(nCmmd);
 
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);
  int from = P.GetIntValueFor(bCmmd);
  int to = P.GetIntValueFor(eCmmd);
  bool bN = P.GetBoolValueFor(nCmmd);
 

  //comment. ???
  FlatFileParser parser;
  
  parser.Open(fileName);

  while (parser.ParseLine()) {
    if (parser.GetItemCount() == 0)
      continue;
  */

  int i, j;

  LuaManip manip;

  manip.Open("scripts/hiv.lua");
  
  AntsGraph ants;

  ants.SetupFullRandom(11);
  ants.SetOptimizeLastCost(true);
  //ants.SetMaxRisk(0.85);
  

  ants[0].Label() = "pause";
  ants[1].Label() = "aggressive";
  ants[2].Label() = "medium";
  ants[3].Label() = "light";
  ants[4].Label() = "pause";
  ants[5].Label() = "aggressive";
  ants[6].Label() = "medium";
  ants[7].Label() = "light";
  ants[8].Label() = "aggressive";
  ants[9].Label() = "medium";
  ants[10].Label() = "light";

  cout << ". ";
  for (i=0; i<ants.isize(); i++) {
    cout << " " << ants[i].Label();
  }

  cout << endl;
  
  for (i=0; i<ants.isize(); i++) {
    //double risk = 0.4*exp((double)i/(double)ants.isize());
    //cout << "Risk: " << risk;
    //ants[i].Risk() = risk;
    cout << ants[i].Label();
    ants[i].Cost() = 0.;
    
    for (j=0; j<ants[i].EdgeCount(); j++) {
      AEdge & edge = ants[i].Edge(j);
      edge.Cost() = 0.; //RandomFloat(0.1);
      edge.Risk() = 0.; //RandomFloat(10);

      /*if (i == 1 || i == 5)
	edge.Cost() = 1.5;
      if (i == 2 || i == 6)
      edge.Cost() = 2.1;*/
      
      //cout << " " << edge.Cost();
    }
    cout << endl;
  }

  
  int a = ants.Types().AddType(false, "pause");
  int b = ants.Types().AddType(false, "aggressive");
  int c = ants.Types().AddType(false, "medium");
  int d = ants.Types().AddType(false, "light");

  
  ants[0].Type() = a;
  ants[4].Type() = a;

  /*ants[1].Type() = b;
  ants[2].Type() = c;
  ants[3].Type() = d;
  ants[4].Type() = a;
  ants[5].Type() = b;
  ants[6].Type() = c;
  ants[7].Type() = d;
  */

  
  ants[0].Risk() = .0;
  ants[1].Risk() = .0;
  ants[2].Risk() = .0;
  ants[3].Risk() = .0;
  ants[4].Risk() = .0;
  ants[5].Risk() = .0;
  ants[6].Risk() = .0;
  ants[7].Risk() = .0;
  ants[8].Risk() = .0;
  ants[9].Risk() = .0;
  ants[10].Risk() = .0;

  ants[0].Cost() = 0.;
  ants[1].Cost() = 0.;
  ants[2].Cost() = 0.;
  ants[3].Cost() = 0.;
  ants[4].Cost() = 0.;
  ants[5].Cost() = 0.;
  ants[6].Cost() = 0.;
  ants[7].Cost() = 0.;
  ants[8].Cost() = 0.;
  ants[9].Cost() = 0.;
  ants[10].Cost() = 0.;

  
  ants[0].Duration() = 1;
  ants[1].Duration() = 2;
  ants[2].Duration() = 3;
  ants[3].Duration() = 2;
  ants[4].Duration() = 1;  
  ants[5].Duration() = 3;  
  ants[6].Duration() = 3;
  ants[7].Duration() = 2;
  ants[8].Duration() = 5;
  ants[9].Duration() = 4;
  ants[10].Duration() = 4;
  
  
  for (i=0; i<ants.isize(); i++)
    ants[i].SetManip(&manip);
  
  RandomFloat(2);
  
  double t = 9999999;

  for (i=0; i<1000; i++) {
    cout << "Round " << i << endl;
    AntsGraph tmp;
    tmp = ants;
    double s = tmp.Run(1);
    cout << "SCORE " << s << endl;
    if (s < t) {
      t = s;
      cout << "********* Minimum: " << t << endl;
      tmp.GetCurrentBestHistory().Print();
      tmp.Print();
      
    }

    /*
    ants[0].Risk() = 0.05;
    ants[1].Risk() = 0.11;
    ants[2].Risk() = 0.2;
    ants[3].Risk() = 0.25;
    ants[4].Risk() = 0.23;
    ants[5].Risk() = 0.17;
    ants[6].Risk() = 0.12;
    ants[7].Risk() = 0.09;
    for (i=0; i<ants.isize(); i++) {
      ants[i].Cost() = 0.;
      }*/
  }
  
  return 0;
}
