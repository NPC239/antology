#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsManip.h"
#include "plugins/ManipLua.h"

const int NODES = 8;


int main( int argc, char** argv )
{

  /*commandArg<string> fileCmmd("-i","input file");
  commandArg<int> bCmmd("-from","from column");
  commandArg<int> eCmmd("-to","to column");
  commandArg<bool> nCmmd("-newline","add newline", false);
  commandLineParser P(argc,argv);
  P.SetDescription("Testing the file parser.");
  P.registerArg(fileCmmd);
  P.registerArg(bCmmd);
  P.registerArg(eCmmd);
  P.registerArg(nCmmd);
 
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);
  int from = P.GetIntValueFor(bCmmd);
  int to = P.GetIntValueFor(eCmmd);
  bool bN = P.GetBoolValueFor(nCmmd);
 

  //comment. ???
  FlatFileParser parser;
  
  parser.Open(fileName);

  while (parser.ParseLine()) {
    if (parser.GetItemCount() == 0)
      continue;
  */

  int i, j;

  LuaManip manip;

  manip.Open("scripts/risk_test.lua");
  
  AntsGraph ants;

  ants.SetupFullRandom(NODES);
  ants.SetOptimizeRisk(true);

  for (i=0; i<ants.isize(); i++)
    ants[i].SetManip(&manip);
  
  RandomFloat(2);
  
  double t = 9999999;

  for (i=0; i<1000; i++) {
    cout << "Round " << i << endl;
    double s = ants.Run(1);
    cout << "SCORE " << s << endl;
    if (s < t) {
      t = s;
      cout << "********* Minimum: " << t << endl;
      ants.Print();
    }
  }
  
  return 0;
}
