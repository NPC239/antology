#include <string>

#include "base/SVector.h"
#include "plugins/LuaWrap.h"
#include "ml/AntsGraph.h"



int main( int argc, char** argv )
{

  LuaState w;

  w.Load("scripts/risk_test.lua");

  svec<double> in, out;

  in.resize(2);
  in[0] = 2;
  in[1] = 3;

  out.resize(2, 0);

  w.Call(in, out, "Testing", "mult");
 

  cout << "TEST: " << out[0] << " " << out[1] << endl;
  

  
  ANode n;
  int index = 7;
  
  n.Label() = "TestNode";
  n.Cycle() = 15;
  n.Duration() = 22;
  n.Cost() = 18.6;
  n.Risk() = 0.128;

  n.Write(in, out);

  in[0] = 4;
  in[1] = index;

  cout << n.Label() << endl;
  cout << n.Duration() << endl;
  cout << n.Cost() << endl;
  cout << n.Risk() << endl;
  
  
  cout << "Call lua." << endl;
  cout << "---------------------------------" << endl;

  for (int i=0; i<in.isize(); i++)
    cout << in[i] << endl;
  
  cout << "---------------------------------" << endl;
  w.Call(out, in, n.Label(), "ants_cycle");
  cout << "---------------------------------" << endl;
  for (int i=0; i<out.isize(); i++)
    cout << out[i] << endl;

  cout << "---------------------------------" << endl;
  n.Read(out);
  
  //cout << n.Label() << endl;
  cout << n.Duration() << endl;
  cout << n.Cost() << endl;
  cout << n.Risk() << endl;
  
  return 0;

  
}

