#include <string>

#include "base/SVector.h"

//#define luac_c
//#define LUA_CORE


//#include "extern/lua-5.4.2/src/lua.hpp"
#include <stdio.h>

//extern "C" {
#include "extern/lua-5.4.2/src/lua.h"
#include "extern/lua-5.4.2/src/linit.h"
#include "extern/lua-5.4.2/src/lualib.h"

// For compilation
#include "extern/lua-5.4.2/src/lauxlib.h"
#include "extern/lua-5.4.2/src/lbaselib.h"
#include "extern/lua-5.4.2/src/loadlib.h"
#include "extern/lua-5.4.2/src/lcorolib.h"
#include "extern/lua-5.4.2/src/liolib.h"
#include "extern/lua-5.4.2/src/ltablib.h"
#include "extern/lua-5.4.2/src/lstrlib.h"
#include "extern/lua-5.4.2/src/lmathlib.h"
#include "extern/lua-5.4.2/src/lutf8lib.h"
#include "extern/lua-5.4.2/src/ldblib.h"

//}


#include <stdlib.h>
#include <stdio.h>



int luaadd ( int x, int y, lua_State* L)
{
	int sum;
	int status;
	
	/* the function name */
	status = lua_getglobal(L, "add");

	cout << status << endl;
	
	/* the first argument */
	lua_pushnumber(L, x);

	/* the second argument */
	lua_pushnumber(L, y);

	/* call the function with 2 arguments, return 1 result */
	lua_call(L, 2, 2);

	/* get the result */
	sum = (int)lua_tointeger(L, -2);
	cout << "First " << sum << endl;
	//lua_pop(L, 1);

	sum = (int)lua_tointeger(L, -1);
	cout << "Second " << sum << endl;
	//lua_pop(L, 1);

	return sum;
}

int luamul ( int x, int y, lua_State* L)
{
	int sum;
	int status;
	
	/* the function name */
	status = lua_getglobal(L, "mult");

	cout << status << endl;
	
	/* the first argument */
	lua_pushnumber(L, x);

	/* the second argument */
	lua_pushnumber(L, y);

	/* call the function with 2 arguments, return 1 result */
	lua_call(L, 2, 1);

	/* get the result */
	sum = (int)lua_tointeger(L, -1);
	lua_pop(L, 1);

	return sum;
}


int main( int argc, char** argv )
{
  	int sum;
	int status;

	lua_State* L;

	/* initialize Lua */
	//L = lua_open();
	L = luaL_newstate();

  
	/* load Lua base libraries */
	luaL_openlibs(L);


	status = luaL_loadfile(L, "add.lua");
	if (status) {
	  /* If something went wrong, error message is at the top of */
	  /* the stack */
	  fprintf(stderr, "Couldn't load file: %s\n", lua_tostring(L, -1));
	  exit(1);
	}

	lua_pcall(L, 0, 0, 0);

	
	/* call the add function */
	sum = luaadd( 10, 15, L);

	/* print the result */
	printf( "The sum is %d\n", sum );

	/* call the add function */
	sum = luamul( 10, 15, L);

	/* print the result */
	printf( "The product is %d\n", sum );

	/* cleanup Lua */
	lua_close(L);

	/* pause */
	printf( "Press enter to exit..." );
	getchar();

	return 0;

  
}

