#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsManip.h"
#include "plugins/ManipLua.h"



int main( int argc, char** argv )
{

  /*commandArg<string> fileCmmd("-i","input file");
  commandArg<int> bCmmd("-from","from column");
  commandArg<int> eCmmd("-to","to column");
  commandArg<bool> nCmmd("-newline","add newline", false);
  commandLineParser P(argc,argv);
  P.SetDescription("Testing the file parser.");
  P.registerArg(fileCmmd);
  P.registerArg(bCmmd);
  P.registerArg(eCmmd);
  P.registerArg(nCmmd);
 
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);
  int from = P.GetIntValueFor(bCmmd);
  int to = P.GetIntValueFor(eCmmd);
  bool bN = P.GetBoolValueFor(nCmmd);
 

  //comment. ???
  FlatFileParser parser;
  
  parser.Open(fileName);

  while (parser.ParseLine()) {
    if (parser.GetItemCount() == 0)
      continue;
  */

  int i, j;

  LuaManip manip;

  manip.Open("scripts/vaccine.lua");
  
  AntsGraph ants;

  ants.SetupFullRandom(8);
  ants.SetOptimizeCost(true);


  ants[0].Label() = "0-10";
  ants[1].Label() = "11-20";
  ants[2].Label() = "21-30";
  ants[3].Label() = "31-40";
  ants[4].Label() = "41-50";
  ants[5].Label() = "51-60";
  ants[6].Label() = "61-70";
  ants[7].Label() = "70+";

  cout << ". ";
  for (i=0; i<ants.isize(); i++) {
    cout << " " << ants[i].Label();
  }

  cout << endl;
  
  for (i=0; i<ants.isize(); i++) {
    //double risk = 0.4*exp((double)i/(double)ants.isize());
    //cout << "Risk: " << risk;
    //ants[i].Risk() = risk;
    cout << ants[i].Label();
    ants[i].Cost() = 0.;
    
    for (j=0; j<ants[i].EdgeCount(); j++) {
      AEdge & edge = ants[i].Edge(j);
      edge.Cost() = 0.; //RandomFloat(0.1);
      edge.Risk() = 0.; //RandomFloat(10);
      
      cout << " " << edge.Cost();
    }
    cout << endl;
  }

  ants[0].Risk() = 0.005;
  ants[1].Risk() = 0.008;
  ants[2].Risk() = 0.10;
  ants[3].Risk() = 0.09;
  ants[4].Risk() = 0.08;
  ants[5].Risk() = 0.010;
  ants[6].Risk() = 0.009;
  ants[7].Risk() = 0.005;

  
  ants[1].Duration() = 1;
  ants[2].Duration() = 1;
  ants[3].Duration() = 2;
  ants[4].Duration() = 3;  
  ants[5].Duration() = 4;  
  ants[6].Duration() = 5;
  ants[7].Duration() = 5;
  
  
  for (i=0; i<ants.isize(); i++)
    ants[i].SetManip(&manip);
  
  RandomFloat(2);
  
  double t = 9999999;

  for (i=0; i<1000; i++) {
    cout << "Round " << i << endl;
    AntsGraph tmp;
    tmp = ants;
    double s = tmp.Run(2);
    cout << "SCORE " << s << endl;
    if (s < t) {
      t = s;
      cout << "********* Minimum: " << t << endl;
      tmp.Print();
    }

    /*
    ants[0].Risk() = 0.05;
    ants[1].Risk() = 0.11;
    ants[2].Risk() = 0.2;
    ants[3].Risk() = 0.25;
    ants[4].Risk() = 0.23;
    ants[5].Risk() = 0.17;
    ants[6].Risk() = 0.12;
    ants[7].Risk() = 0.09;
    for (i=0; i<ants.isize(); i++) {
      ants[i].Cost() = 0.;
      }*/
  }
  
  return 0;
}
