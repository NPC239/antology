#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsJson.h"
#include "plugins/ManipLua.h"
#include "ml/HyperVec.h"
#include "base/StringUtil.h"

int main( int argc, char** argv )
{

  /* commandArg<string> fileCmmd("-i","input graph file, text or json");
  commandArg<string> outCmmd("-o","output graph file, text or json", "out.json");
  commandArg<string> luaCmmd("-lua","lua script", "");
  commandArg<int> aCmmd("-ants", "number of cocurrent ants", 1);
  commandArg<int> nCmmd("-n", "iterations", 25);
  commandArg<double> maxTCmmd("-maxtime", "maximum allowed time", -1.);
  commandArg<double> maxCCmmd("-maxcost", "maximum allowed cost", -1.);
  commandArg<double> maxRCmmd("-maxrisk", "maximum allowed risk", -1.);
  commandArg<int> optCmmd("-by", "what to optimize by: 0 = time, 1 = cost, 2 = risk", 0);
  commandLineParser P(argc,argv);
  P.SetDescription("ANTology main executable.");
  P.registerArg(fileCmmd);
  P.registerArg(outCmmd);
  P.registerArg(luaCmmd);
  P.registerArg(aCmmd);
  P.registerArg(nCmmd);
  P.registerArg(maxTCmmd);
  P.registerArg(maxCCmmd);
  P.registerArg(maxRCmmd);
  P.registerArg(optCmmd);
 
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);
  string outName = P.GetStringValueFor(outCmmd);
  string luaName = P.GetStringValueFor(luaCmmd);
  int num_ants = P.GetIntValueFor(aCmmd);
  int N = P.GetIntValueFor(nCmmd);
  double maxTime = P.GetDoubleValueFor(maxTCmmd);
  double maxCost = P.GetDoubleValueFor(maxCCmmd);
  double maxRisk = P.GetDoubleValueFor(maxRCmmd);
  int by = P.GetIntValueFor(optCmmd);
  */
 

  int i, j;

  AntsGraph ants;

  int num = 60;

  svec<HyperVector> hd;

  HyperVector test;
  
  for (i=0; i<num; i++) {
    ANode n;
    string l = "hd_" + Stringify(i);
    n.Label() = l;
    ants.push_back(n);
    
    HyperVector h;
    h.RandomInit();
    hd.push_back(h);
  }

  int k =0;
  for (i=0; i<num; i++) {
    int to = (i+1) % num;
    hd[to].MergeRand(hd[i], 0.4);
    cout << i << "\t" << to << endl;
  }
  
  for (i=0; i<num; i++) {
    for (j=0; j<num; j++) {
      if (i == j)
	continue;
      double d = 1. - hd[i].SimpleDot(hd[j]);
      cout << i << "\t" << j << "\t" << d << endl;
      ants[i].AddEdge(j, 1, d, 0.);
    }
  }

  test.MergeRand(hd[45], 0.1);
  
  double t = 9999999;

  int N = 100;
  int num_ants = 1;
  
  ants.SetOptimizeCost(true);
  AHistory hist;
  
  for (i=0; i<N; i++) {
    cout << "Round " << i << endl;
    double s = ants.Run(num_ants);
    if (s < t) {
      t = s;
      cout << "********* Minimum: " << t << endl;
      ants.Print();
      hist = ants.GetCurrentBestHistory();
    }
  }

  cout << "Done" << endl;
  

  double min = 2.;
  int idx = -1;
  int idxA = -1;
  for (i=0; i<hd.isize(); i++) {
    double d = 1. - hd[hist[i]].SimpleDot(test);
    string ann;
    if (i % 10 == 0)
      ann = " <-";
    cout << hist[i] << "\t" << ants[hist[i]].Label() << "\t" << d << ann << endl;
    if (d < min) {
      min = d;
      idxA = i;
      idx = hist[i];
    }
  }

  cout << "Best match, index: " << idxA << " #: " << idx << " -> " << min << endl;

  //double dd0 = 1. - hd[0].SimpleDot(test);
  //double dd0 = 1. - hd[0].SimpleDot(test);
  //double dd0 = 1. - hd[0].SimpleDot(test);


  return 0;
}
