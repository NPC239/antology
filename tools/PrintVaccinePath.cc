#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsManip.h"
#include "plugins/ManipLua.h"



int main( int argc, char** argv )
{

  commandArg<string> fileCmmd("-i","Node order, e.g.: 0,4,5,6");
  commandLineParser P(argc,argv);
  P.SetDescription("Testing the file parser.");
  P.registerArg(fileCmmd);
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);

  StringParser p;
  p.SetLine(fileName, ",");

  int i, j;

  svec<int> force;
  force.resize(p.GetItemCount(), 0);

  for (i=0; i<p.GetItemCount(); i++)
    force[i] = p.AsInt(i);
 


  
  LuaManip manip;

  manip.Open("scripts/vaccine.lua");
  
  AntsGraph ants;

  ants.SetupFullRandom(8);
  ants.SetOptimizeCost(true);

  for (i=0; i<ants.isize(); i++) {
    //double risk = 0.4*exp((double)i/(double)ants.isize());
    //cout << "Risk: " << risk;
    //ants[i].Risk() = risk;
    ants[i].Cost() = 0.;
    for (j=0; j<ants[i].EdgeCount(); j++) {
      AEdge & edge = ants[i].Edge(j);
      edge.Cost() = 0.; //RandomFloat(0.1);
      edge.Risk() = 0.; //RandomFloat(10);
    }
  }

  
  ants[0].Risk() = 0.005;
  ants[1].Risk() = 0.008;
  ants[2].Risk() = 0.10;
  ants[3].Risk() = 0.09;
  ants[4].Risk() = 0.08;
  ants[5].Risk() = 0.010;
  ants[6].Risk() = 0.009;
  ants[7].Risk() = 0.005;

  ants[1].Duration() = 1;
  ants[2].Duration() = 1;
  ants[3].Duration() = 2;
  ants[4].Duration() = 3;  
  ants[5].Duration() = 4;  
  ants[6].Duration() = 5;
  ants[7].Duration() = 5;

  for (i=0; i<ants.isize(); i++)
    ants[i].SetManip(&manip);
  
  RandomFloat(2);
  
  double t = 9999999;

 
  ants.SetForce(force);
  
  double s = ants.Run(1);
  cout << "SCORE " << s << endl;
    
     
  //cout << "********* Minimum: " << t << endl;
  ants.Print();
   

    
  return 0;
}
