#include <string>
#include "base/CommandLineParser.h"
#include "base/FileParser.h"
#include "base/RandomStuff.h"
#include "ml/AntsGraph.h"
#include "ml/AntsManip.h"

const int NODES = 8;

class VirusManip : public IAntsManip
{
 public:
  virtual ~VirusManip() {}

  virtual void OnCycle(int timestamp, int index, ANode & n) {

    /*
    if (timestamp == index) {
      //cout << "CYCLE Set time " << timestamp << " node " << index << endl;
      n.Risk() = 0.15;
    }
       
    return;*/
    
    double f = (double)index/(double)NODES;

    f *= ((double)timestamp);

    f /= 4.;
    
    double risk = 1. - n.Risk();

    risk *= (1. - f);

    risk = 1. - risk;
    
    //cout << "Update cycle " << timestamp << " node " << index << " before: " << n.Risk() << " after: " << risk << endl;

    n.Risk() = risk;
    
  }
  virtual void StartCycle(int index, ANode & n)
  {
    n.Risk() = 0.;
  }

  virtual void EndCycle(int index, ANode & n) {
  }


};


int main( int argc, char** argv )
{

  /*commandArg<string> fileCmmd("-i","input file");
  commandArg<int> bCmmd("-from","from column");
  commandArg<int> eCmmd("-to","to column");
  commandArg<bool> nCmmd("-newline","add newline", false);
  commandLineParser P(argc,argv);
  P.SetDescription("Testing the file parser.");
  P.registerArg(fileCmmd);
  P.registerArg(bCmmd);
  P.registerArg(eCmmd);
  P.registerArg(nCmmd);
 
  P.parse();
  
  string fileName = P.GetStringValueFor(fileCmmd);
  int from = P.GetIntValueFor(bCmmd);
  int to = P.GetIntValueFor(eCmmd);
  bool bN = P.GetBoolValueFor(nCmmd);
 

  //comment. ???
  FlatFileParser parser;
  
  parser.Open(fileName);

  while (parser.ParseLine()) {
    if (parser.GetItemCount() == 0)
      continue;
  */

  int i, j;

  VirusManip manip;
  
  AntsGraph ants;

  ants.SetupFullRandom(NODES);
  ants.SetOptimizeRisk(true);

  for (i=0; i<ants.isize(); i++)
    ants[i].SetManip(&manip);
  
  RandomFloat(2);
  
  double t = 9999999;

  for (i=0; i<1000; i++) {
    cout << "Round " << i << endl;
    double s = ants.Run(1);
    cout << "SCORE " << s << endl;
    if (s < t) {
      t = s;
      cout << "********* Minimum: " << t << endl;
      ants.Print();
    }
  }
  
  return 0;
}
