#ifndef LUAMANIP_H
#define LUAMANIP_H

#include "ml/AntsManip.h"
#include "ml/AntsGraph.h"
#include "plugins/LuaWrap.h"



class LuaManip : public IAntsManip
{
 public:
  virtual ~LuaManip() {}

  void Open(const string & fileName) {
    m_lua.Load(fileName);
  }
  
  virtual void StartCycle(int index, ANode & n) {
    svec<double> in, out;
    n.Write(in, out);
    m_lua.Call(out, in, n.Label(), "ants_round_start");
    n.Read(out);

  }
  
  virtual void OnCycle(int timestamp, int index, ANode & n)
  {
    svec<double> in, out;
    n.Write(in, out);
    in[0] = index;
    in[1] = timestamp;
    m_lua.Call(out, in, n.Label(), "ants_cycle");
    n.Read(out);
  }

  virtual void EndCycle(int index, ANode & n) {
    svec<double> in, out;
    n.Write(in, out);
    m_lua.Call(out, in, n.Label(), "ants_round_end");
    n.Read(out);
  }

 private:
  LuaState m_lua;
  
};


#endif


