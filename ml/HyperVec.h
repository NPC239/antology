#ifndef HYPERVEC_H
#define HYPERVEC_H

#include "base/SVector.h"
#include <math.h>
#include "base/RandomStuff.h"
#include "util/mutil.h"
#include "math/Matrix.h"


class HyperVector
{
 public:
  HyperVector();

  void RandomInit();
  
  int isize() const {return m_data.isize();}
  char & operator [] (int i) {return m_data[i];}
  const char & operator [] (int i) const {return m_data[i];}
  void resize(int n, char v = 0) {
    m_data.resize(n, v);
  }
  
  HyperVector operator + (const HyperVector & h) const {
    HyperVector w = *this;
    w += h;
    return w;
  }
  
  HyperVector operator - (const HyperVector & h) const {
    HyperVector w = *this;
    w -= h;
    return w;
  }

  double operator * (const HyperVector & h) const;

  double Dot(const HyperVector & h, int from, int to) const;

  double SimpleDot(const HyperVector & h) const {
    int s = 0.;
    for (int i=0; i<isize(); i++) {
      //cout << (int)m_data[i] << " " << (int)h[i] << endl;
      s += m_data[i] * h[i];
    }
    //cout << s << endl;
    return (double)s/(double)isize();
  }
  
  void Set(const HyperVector & h, int from, int to) {
    for (int i=from; i<to; i++)
      m_data[i] = h[i];
  }
  
  void operator += (const HyperVector & h) {
    for (int i=0; i<m_data.isize(); i++) {
      m_data[i] += h[i];
    }
  }
  
  void operator -= (const HyperVector & h) {
    for (int i=0; i<m_data.isize(); i++) {
      m_data[i] -= h[i];
    }
  }

  void Normalize() {
    int i;
    for (i=0; i<m_data.isize(); i++) {
      if (m_data[i] > 0)
	m_data[i] = 1;
      if (m_data[i] < 0)
	m_data[i] = -1;
      if (m_data[i] == 0) {
	if (RandomFloat(1) > 0.5)
	  m_data[i] = 1;
	else
	  m_data[i] = -1;
      }
    }
  }
  
 
  void operator *= (const HyperVector & h) {
    for (int i=0; i<m_data.isize(); i++) {
      m_data[i] *= h[i];
    }  
  }

  void XOR(const HyperVector & h) {
    for (int i=0; i<m_data.isize(); i++) {
      //cout << "XOR pre:  " << (int) h[i] << " " << (int)m_data[i] << endl;
      m_data[i] = -(h[i] * m_data[i]);
      //cout << "XOR post: " << (int)m_data[i] << endl;
    }  
  }

  void Merge(const HyperVector & h, double weight = 0.5) {
    int i;
    int n = m_data.isize()*weight;
    if (n > m_data.isize())
      n = m_data.isize();
    for (i=0; i<n; i++) {
      m_data[i] = h[i];
    }
    //cout << "Weight=" << weight << ", merged: " << n << endl;
  }
  
  void Multiply(const HyperVector & h, double weight = 1.0) {
    for (int i=0; i<m_data.isize(); i++) {
      if (RandomFloat(1) > weight)
	continue;
      m_data[i] *= h[i];
    }  
  }
  
  void MultiplyReverse(const HyperVector & h, double weight = 1.0) {
    for (int i=0; i<m_data.isize(); i++) {
      if (RandomFloat(1) > weight)
	m_data[i] = h[i];
      else
	m_data[i] *= h[i];
    }  
  }
  
  void MergeRand(const HyperVector & h, double weight = 0.5, int from = 0, int to = -1) {
    int i;
    int n = 0;

    if (to == -1)
      to = m_data.isize();
    
    for (i=from; i<to; i++) {
      //if (m_data[i] == h[i])
      //continue;
      if (RandomFloat(1) < weight) {
	m_data[i] = h[i];
	n++;
      }
    }
    //cout << "Weight=" << weight << ", merged: " << n << endl;
  }

  double GetVal(const svec<double> & f, double x) {
    int a = (int)(x*f.isize()-1);
    double w = ((x*(double)(f.isize()-1)) - a);
    
    double v = f[a]*(1.-w);
    if (a < f.isize()-1)
      v += f[a+1]*w;
    return v;
  }

  //====================================================================
  // Dynamic range is -1 to 1, weights are interpolated equidistantly
  // NOTE: Extend this to an array of orthogonal plusses (i.e. h[])
  void MergeRand(const HyperVector & zero, const HyperVector & h, const svec<double> & weights, double w = 1.) {
    int i;
    int n = 0;
    
    int j = 0;
    
    // Fast-track equal size
    if (weights.isize() == m_data.isize()) {
      for (i=0; i<m_data.isize(); i++) {
	if (RandomFloat(1) > w)
	  continue;
	
	if (weights[i] >= 0.) {
	  if (RandomFloat(1) < weights[i]) {
	    m_data[i] = h[i];
	    n++;
	  } else {
	    m_data[i] = zero[i];
	  }
	} else {
	  if (RandomFloat(1) < -weights[i]) {
	    m_data[i] = -h[i];
	    n++;
	  } else {
	    m_data[i] = zero[i];
	  }
	}
      }
    } else {
  
      for (i=0; i<m_data.isize(); i++) {
	if (RandomFloat(1.) > w)
	  continue;
	
	double v = GetVal(weights, (double)i/(double)(m_data.isize()-1));
	if (v >= 0.) {
	  if (RandomFloat(1) < v) {
	    m_data[i] = h[i];
	    n++;
	    //cout << "Plus " << v << endl;
	  } else {
	    m_data[i] = zero[i];
	  }
	} else {
	  if (RandomFloat(1) < -v) {
	    m_data[i] = -h[i];
	    n++;
	    //cout << "Minus " << -v << endl;
	  } else {
	    m_data[i] = zero[i];
	  }
	}
      }
    }
  }

  // Use the one above!!
  void MergeRand(const HyperVector & h, const svec<double> & weights, double w = 1.) {
    int i;
    int n = 0;
    
    int j = 0;
    
    // Fast-track rqual size
    if (weights.isize() == m_data.isize()) {
      for (i=0; i<m_data.isize(); i++) {
	if (weights[i] >= 0.) {
	  if (RandomFloat(1) < weights[i]*w) {
	    m_data[i] = h[i];
	    n++;
	  }
	} else {
	  if (RandomFloat(1) < -weights[i]*w) {
	    m_data[i] = -h[i];
	    n++;
	  }
	}
      }
    } else {
  
      for (i=0; i<m_data.isize(); i++) {
	double v = GetVal(weights, (double)i/(double)(m_data.isize()-1));
	if (v >= 0.) {
	  if (RandomFloat(1) < v*w) {
	    m_data[i] = h[i];
	    n++;
	    //cout << "Plus " << v << endl;
	  }
	} else {
	  if (RandomFloat(1) < -v*w) {
	    m_data[i] = -h[i];
	    n++;
	    //cout << "Minus " << -v << endl;
	  }
	}
      }
    }
  }
  

  
  void Rotate(int n);

  void Print() const;
  void Print(const HyperVector & h) const;

  // n is the multiple of 2  
  void Fold(int n);
  void UnFold(int n);

  double & Hits() {return m_hits;}
  const double & Hits() const {return m_hits;}

  void Read(IMReadStream &s);
  void Write(IMWriteStream &s) const;
  
 private:
  svec<char> m_data;
  double m_hits;

};


class MajorityVote
{
 public:
  MajorityVote() {}

  void Add(const HyperVector & h, double weight = 1.) {
    m_vec.push_back(h);
    m_weight.push_back(weight);
  }

  void Clear() {m_vec.clear();}

  HyperVector Vote() const;
  
  HyperVector Vote(const svec<HyperVector> & v, const svec<double> & weight) const;

   
 private:
  svec<HyperVector> m_vec;
  svec<double> m_weight;
};

//======================================================
class RandomMerge
{
 public:
  RandomMerge(int size = 0) {
    m_index.resize(size, 0);
    Init();
  }

  void resize(int i) {
    m_index.resize(i, 0);
    Init();
  }
  
  void Init() {
    int i;
    for (i=0; i<m_index.isize(); i++) {
      m_index[i] = i;
    }
    SRandomize(m_index);
  }

  HyperVector Merge(const HyperVector & one, const HyperVector & two, double w) const;
  
 private:
  svec<int> m_index;
};

//--------------------------------------------------------------------------
// A set of HV 
class HyperVecPool
{
public:
  HyperVecPool() {}

  int isize() const {return m_data.isize();}
  const HyperVector & operator[] (int i) const {return m_data[m_index[i]];}
  HyperVector & operator[] (int i) {return m_data[m_index[i]];}
  void push_back(const HyperVector & h) {
    m_index.push_back(m_data.isize());
    m_data.push_back(h);
  }
  
  HyperVector & add() {
    HyperVector h;
    h = m_null;
    m_index.push_back(m_data.isize());
    m_data.push_back(h);
    return m_data[m_data.isize()-1];
  }
  
  void resize(int n) {
    m_data.resize(n);
    m_index.resize(n);
    for (int i=0; i<n; i++) {
      m_data[i] = m_null;
      m_index[i] = i; 
    }
  }
  
  void Randomize() {
    for (int i=0; i<m_data.isize(); i++) {
      m_data[i].RandomInit();
    }
  }

  const HyperVector & Null() const {return m_null;}
  HyperVector & Null() {return m_null;}
  const svec<HyperVector> & Dimensions() const {return m_dim;}
  svec<HyperVector> & Dimensions() {return m_dim;}
  void AddDimension() {
    HyperVector h;
    h.RandomInit();
    m_dim.push_back(h);
  }
  
  void Read(IMReadStream &s);
  void Write(IMWriteStream &s) const;

  void Read(const string & fileName);
  void Write(const string & fileName) const;

  
  void MakeDistMatrix(Matrix & m) const;

  void ReIndex(const svec<int> & idx);

  void MergeIn(int i, const svec<double> & val, double weight = 1., int dimIdx = 0) {
    (*this)[i].MergeRand(m_null, m_dim[dimIdx], val, weight);
  } 
  
private:
  svec<int> m_index;
  svec<HyperVector> m_data;
  HyperVector m_null;
  svec<HyperVector> m_dim;
};





//======================= OBSOLETE?? =========================
//======================================================
class HyperSet
{
 public:
  HyperSet() {
    m_lo = 0.;
    m_fac = 1.;
  }

  void Setup(int n, double warp, int spacing, double low = 0., double high = 1.); 

  void SetRange(double low = 0., double high = 1.) {
    m_lo = low;
    m_fac = 1./(high-low);
  }

  const HyperVector & Get(double w) const {
    int i = (int)(0.5+(w-m_lo)*m_fac*(m_vec.isize()-1));    
    return m_vec[i];
  }

  void Print() const;
  
 private:
  svec<HyperVector> m_vec;
  double m_lo;
  double m_fac;
  HyperVector m_one;
  HyperVector m_zero;
   
};


//=======================================================
class HyperVecModel
{
 public:
  HyperVecModel() {
    //SetSize(0);
    m_hits = 0;
  }
  HyperVecModel(int dim, int n = -1) {
    m_hits = 0;
    SetSize(dim, n);
  }

  void SetSize(int dim, int n = -1);
 
  void Add(const svec<double> & data);
  void Done();

  const HyperVector & Model() const {return m_model;}

  double Cosine(const svec<double> & s) const;

  int & Hits() {return m_hits;}
  const int & Hits() const {return m_hits;}
 private:
  int m_n;
  svec<RandomMerge> m_rm;
  HyperVector m_key;
  HyperVector m_model;
  svec<HyperVector> m_bottom;
  svec<HyperVector> m_top;
  MajorityVote m_vote;
  int m_hits;


};


class HVDataPoint
{
 public:
  HVDataPoint() {
    m_model = 0;
  }

  void resize(int n) {
    m_data.resize(n, 0.);
  }

  double & operator [] (int i) {return m_data[i];}
  const double & operator [] (int i) const {return m_data[i];}
  int isize() const {return m_data.isize();}

  int & Model() {return m_model;}
  const int & Model() const {return m_model;}
  svec<double> & Data() {return m_data;}
  const svec<double> & Data() const {return m_data;}

 private:
  svec<double> m_data;
  int m_model;
};


//===================================================
class MetaHVModel
{
 public:
  MetaHVModel() {
    m_n = -1;
    m_dim = 0;
    m_minPer = 1;
  }
  
  MetaHVModel(int dim, int n = -1) {
    m_minPer = 1;
    SetSize(dim, n);
  }

  int & MinPerModel() {return m_minPer;}
  const int & MinPerModel() const {return m_minPer;}
  
  
  void SetSize(int dim, int n = -1) {
    m_n = n;
    m_dim = dim;
  }
 
  void Add(const svec<double> & data);
  void Train(int max);
 
  double BestCosine(const svec<double> & data) const;
  double BestCosine(int & model, const svec<double> & data) const;

 private:

  int HighestError(double & thresh) const;
  
  svec<HyperVecModel> m_models;
  int m_minPer;
  svec<HVDataPoint> m_data;
  int m_dim;
  int m_n;
  
};



//=============================================================

class HyperVectorNumber
{
 public:
  HyperVectorNumber() {
    m_zero.RandomInit();
    m_one.RandomInit();
    Divide(1);
  }

  void operator = (double d) {
    m_num = m_zero;
    m_num.MergeRand(m_one, d);
  }

  int isize() const {return m_n;}

  double operator[] (int i) const {return Get(i);} 
  
  HyperVector & Vec() {return m_num;}
  const HyperVector & Vec() const {return m_num;}
  
  void Divide(int n = 16) {
    m_n = n;
    m_on.resize(m_n, 1);
  }

  void SetValid(int i, int on) {
    m_on[i] = on;
  }
  
  int N() const {return m_n;}

  // This is by position!
  bool Valid(int i) {
    int idx = i/m_n;
    return m_on[idx];
  }
  
  void Set(int i, double d) {
    int from = i*(m_num.isize()/m_n);
    int to = (i+1)*(m_num.isize()/m_n);
    m_num.Set(m_zero, from, to);
    m_num.MergeRand(m_one, d, from, to);
  }
  
  double Get(int i) const {
    int from = i*(m_num.isize()/m_n);
    int to = (i+1)*(m_num.isize()/m_n);
    double d = m_one.Dot(m_num, from, to);
    return d;
  }
  
 private:
  HyperVector m_zero;
  HyperVector m_one;
  HyperVector m_num;
  svec<int> m_on;
  int m_n;
};

//=============================================================

//=============================================================

class HyperVectorMult
{
 public:
  HyperVectorMult() {
    m_n = 1;
  }

  void SetSize(int n) {
    m_mult.resize(n);
    for (int i=0; i<n; i++)
      m_mult[i].RandomInit();
  }

  int isize() const {return m_n;}

  HyperVector & Vec() {return m_num;}
  const HyperVector & Vec() const {return m_num;}
  
  void Divide(int n = 16) {
    m_n = n;
  }

  int N() const {return m_n;}

  void Set(int i, int num) {
    int from = i*(m_num.isize()/m_n);
    int to = (i+1)*(m_num.isize()/m_n);
    m_num.Set(m_mult[num], from, to);   
  }

  int operator[] (int i) const {return Get(i);}

  void GetAll(svec<double> & out, int i) const {
    int from = i*(m_num.isize()/m_n);
    int to = (i+1)*(m_num.isize()/m_n);
   
    out.resize(m_mult.isize(), 0.);
    
    //cout << from << " " << to << endl;
    for (int j=0; j<m_mult.isize(); j++) {
      double d = m_mult[j].Dot(m_num, from, to);
      out[i] = d;
    }
  
  }

  int Test(int i, const HyperVector & test) {
    int from = i*(m_num.isize()/m_n);
    int to = (i+1)*(m_num.isize()/m_n);
    double max = 0.;
    int idx = -1;
    //cout << from << " " << to << endl;
    for (int j=0; j<m_mult.isize(); j++) {
      double d = m_mult[j].Dot(test, from, to);
      //cout << j << " " << d << endl;
      if (d > max) {
	max = d;
	idx = j;
      }
    }
    //cout << "return " << idx << endl;
    return idx;
 
  }
  
  int Get(int i) const {
    int from = i*(m_num.isize()/m_n);
    int to = (i+1)*(m_num.isize()/m_n);
    double max = 0.;
    int idx = -1;
    //cout << from << " " << to << endl;
    for (int j=0; j<m_mult.isize(); j++) {
      double d = m_mult[j].Dot(m_num, from, to);
      //cout << j << " " << d << endl;
      if (d > max) {
	max = d;
	idx = j;
      }
    }
    //cout << "return " << idx << endl;
    return idx;
  }
  
 private:
  
  svec<HyperVector> m_mult;
  HyperVector m_num;
  int m_n;
};



class HVPair
{
public:
  HVPair(double d, int i) {
    m_d = d;
    m_i = i;
  }

  double m_d;
  double m_i;

  // Reverse
  bool operator < (const HVPair & p) const {
    return -m_d < -p.m_d;
  }
};


class HVAssocMem
{
public:
  HVAssocMem() {
    m_frac = 0.2;
  }

  void SetFrac(double d) {
    m_frac = d;
  }
  
  void SetSize(int n) {
    m_mem.resize(n);
    for (int i=0; i<n; i++)
      m_mem[i].RandomInit();
  }

  int Size() const {return m_mem.isize();}
  
  void Store(svec<int> & index, const HyperVector & v, double ratio, bool bRand = false) {
    int i;
    index.clear();

    if (!bRand) {
      Collect(index, v, ratio, true);
    } else {
      int n = (int)(ratio*(double)m_mem.isize());
      for (i=0; i<n; i++)
	index.push_back(RandomInt(m_mem.isize()));
    }

    cout << "Merge into " << index.isize() << " vectors." << endl;
    for (i=0; i<index.isize(); i++) {
      m_mem[index[i]].MergeRand(v, m_frac);
      m_mem[index[i]].Hits() += 1.;
    }
    
    /*
    for (i=0; i<m_mem.isize(); i++) {
      if (RandomFloat(1.) < ratio) {	
	m_mem[i].MergeRand(v, m_frac);
	index.push_back(i);
      }
      }*/
  }

  void StoreByIndex(const HyperVector & v, double ratio, const svec<int> & index, double weight) {
    int i;
  
    cout << "Merge into " << index.isize() << " FIXED vectors." << endl;
    for (i=0; i<index.isize(); i++) {
      m_mem[index[i]].MergeRand(v, m_frac * weight);
      m_mem[index[i]].Hits() += 1.;
    }
    
  }

  
  void RetrieveBlind(HyperVector & out, const HyperVector & in, double ratio) {
    svec<int> index;
    Collect(index, in, ratio);
    Retrieve(out, index);
  }
  
  void Retrieve(HyperVector & out, const svec<int> & index) {
    if (index.isize() == 0) {
      cout << "WARNING: empty index given!" << endl;
      return;
    }
    
    MajorityVote vote;
    int i;
    for (i=0; i<index.isize(); i++) {
      vote.Add(m_mem[index[i]]);
    }
    out = vote.Vote();
  } 

  void Collect(svec<int> & index, int from, double ratio) {
    Collect(index, m_mem[from], ratio);
  }

  void Collect(svec<int> & index, const HyperVector & in, double ratio, bool forStoring = false) {
    
    int i;

    index.clear();
    
    svec<HVPair> dot;
     
    for (i=0; i<m_mem.isize(); i++) {     
      double d = in * m_mem[i];
      if (forStoring)
	d -= m_mem[i].Hits();
      
      dot.push_back(HVPair(d, i));
    }
    Sort(dot);
    int n = (int)(ratio*(double)dot.isize());
    
    for (i=0; i<n; i++) {
      index.push_back(dot[i].m_i);
      //cout << "Hits: " << m_mem[dot[i].m_i].Hits() << " in HV " << dot[i].m_i << " score: " << dot[i].m_d << endl;
    }

    Sort(index);
  }

  //HyperVector & operator[] (int i) {return m_mem[i];}
  const HyperVector & operator[] (int i) const {return m_mem[i];}
  int isize() const {return m_mem.isize();}
  
private:
  svec<HyperVector> m_mem;
  double m_frac;
};



#endif // HYPERVEC_H

