#ifndef ANTSMANIP_H
#define ANTSMANIP_H

#include "base/SVector.h"

class ANode;


class IAntsManip
{
 public:
  virtual ~IAntsManip() {}

  virtual void StartCycle(int index, ANode & n) = 0;
  
  virtual void OnCycle(int timestamp, int index, ANode & n) = 0;

  virtual void EndCycle(int index, ANode & n) = 0;

};


#endif


