#include "ml/AntsGraph.h"
#include "base/RandomStuff.h"
#include "base/FileParser.h"
#include "ml/AntsManip.h"



void AntsGraph::Print() const
{
  int i, j;

  cout << "--------- Init node:" << endl;
  cout << "index\tedgetime\tphero\t\tcost\trisk" << endl;
  for (i=0; i<m_init.Phero().isize(); i++) {
    cout << m_init.Index()[i] << "\t" << m_init.Edge(i).Duration() << "\t" << m_init.Phero()[i];
    cout << "\t" << m_init.Edge(i).Cost() << "\t" << m_init.Edge(i).Risk() << endl;
  }
  cout << endl;
  
  for (j=0; j<m_nodes.isize(); j++) {
    cout << "---- Node: " << j << endl;
    cout << "label:    " << m_nodes[j].Label() << endl;
    cout << "duration: " << m_nodes[j].Duration() << endl;
    cout << "cost:     " << m_nodes[j].Cost() << endl;
    cout << "risk:     " << m_nodes[j].Risk() << endl;
    cout << "type:     " << m_nodes[j].Type() << endl;
    cout << "--------- Edges: " << m_nodes[j].Phero().isize() << endl;
    cout << "index\tedgetime\tphero\tcost\trisk" << endl;
    for (i=0; i<m_nodes[j].Phero().isize(); i++) {
      cout << m_nodes[j].Index()[i] << "\t" << m_nodes[j].Edge(i).Duration() << "\t" << m_nodes[j].Phero()[i];
      cout << "\t" << m_nodes[j].Edge(i).Cost()  << "\t" << m_nodes[j].Edge(i).Risk() << endl;
    }
    cout << endl;
  }

}


void AntsGraph::SetupFullRandom(int tasks)
{
  m_nodes.resize(tasks);
  int i, j;

  double maxrisk = 0.945;
  maxrisk = 0.01;
  double d = 1.;
  for (i=0; i<m_nodes.isize(); i++)
    d *= maxrisk;
 

  //cout << "Max risk: " << maxrisk << endl;
  //maxrisk = 0.2;
  
  for (i=0; i<tasks; i++) {
    m_nodes[i].Duration() = RandomInt(50);
    m_nodes[i].Duration() = 1;
    m_nodes[i].Risk() = 0.;
    m_init.AddEdge(i, 0, 0, 0);
    for (j=0; j<tasks; j++) {
      if (j == i+1) {
	//m_nodes[i].AddEdge(j, 0, 0, 0);
	//m_nodes[i].AddEdge(j, RandomInt(150), 5 + RandomFloat(150), 0);
	m_nodes[i].AddEdge(j, 0, 0, RandomFloat(d));
      } else {
	//if (i == tasks-1)
	//m_nodes[i].AddEdge(0, 0, 0, 0);
	//else
	//m_nodes[i].AddEdge(j, RandomInt(150), 5 + RandomFloat(150), RandomFloat(d));
	m_nodes[i].AddEdge(j, 0, 0, RandomFloat(d));
      }
    }
  }
  
}

void AntsGraph::Randomize()
{
  int i, j;

  for (i=0; i<m_nodes.isize(); i++) {
    m_nodes[i].Duration() = RandomInt(200);
  }
  
}

void ANode::Decay() {
  //double decay = 0.96;
  double decay = 0.97;
  //cout << "Before " << m_phero[idx] << endl;
  for (int i=0; i<m_phero.isize(); i++) {
    m_phero[i] *= decay;
    if (m_phero[i] < 0.1)
      m_phero[i] = 0.1;
  }
}

void ANode::Learn(int idx, double diff) {
  int i;
  int index = GetEdgeFor(idx);
  
  m_phero[index] += 0.015 + 0.01*diff;
  //cout << "After " << m_phero[idx] << endl;
}



void AntsGraph::Learn(int antnum, double diff, const AHistory & hist)
{
  int i, j;

  for (i=0; i<m_nodes.isize(); i++) {
    m_nodes[i].Decay();
  }
  m_init.Decay();
  
  
  //cout << "Antnum: " << antnum << endl;
  for (i=0; i<antnum; i++) {    
    int curr = 0;
    int last = -1;
    for (j=0; j<hist.isize(); j++) {
      //cout << i << " - " << hist.isize() << endl;
      if (hist.Ant(j) == i) {
	if (curr == 0) {
	  m_init.Learn(hist[j], diff);
	} else {
	  //cout << last << " " << hist[j] << " " << diff << endl;
	  m_nodes[last].Learn(hist[j], diff);
	}

	last = hist[j];      
	curr++;

      }
      
    }
  }
  //cout << "Done learn" << endl;
}

double AntsGraph::Run(int antnum)
{
  int i, j;
  double score = -1;

  //double slack = 1.35; //1.2
  double slack = 1.45; //1.2

  for (i=0; i<m_nodes.isize(); i++) {
    m_nodes[i].ResetPhero(0.2);
    //m_nodes[i].ResetSoft();
  }
  
  int n = 1000;
  if (m_forced.isize() > 0)
    n = 1;
  
  for (i=0; i<n; i++) {
    AHistory hist;
    //cout << "Start run... " << endl;

    for (j=0; j<m_nodes.isize(); j++) {
      if (m_nodes[j].GetManip() != NULL) {
	m_nodes[j].GetManip()->StartCycle(j, m_nodes[j]);
      }
    }
 

    //cout << "COST for prod (pre):  " << m_nodes[11].Cost() << endl;
    double s = OneRun(antnum, hist);
    //cout << "COST for prod (post): " << m_nodes[11].Cost() << endl;

    
    for (j=0; j<m_nodes.isize(); j++) {
      if (m_nodes[j].GetManip() != NULL) {
	m_nodes[j].GetManip()->EndCycle(j, m_nodes[j]);
      }
    }

    //cout << "ONE " << s << " " << score << endl;
    if (score < 0)
      score = s;

    //==============================================
    //hist.Print();

   
    if (m_maxTime > 0. && hist.TotalTime() > m_maxTime)
      return score;
    if (m_maxCost > 0. && hist.TotalCost() > m_maxCost)
      return score;
    if (m_maxRisk > 0. && hist.TotalRisk() > m_maxRisk)
      return score;

    
    if (s < score*slack && s < inf_score-1) {
      double w = score*slack - s;

      
      if (m_bRisk) {	
	w = m_riskScale/(1.001 - score);   //m_riskScale;
      }
   

      
      //w *= 0.5;
      //cout << "Learning" << endl;
      Learn(antnum, w, hist);
      if (s <= score) {
	score = s;
	hist.Print();
	//cout << i << " Learn, score: " << s << " weight " << w << endl;
	m_bestHist = hist;
      }
    }
  }

  return score;
}

double AntsGraph::OneRun(int antnum, AHistory & hist) 
{
  //cout << "----- OneRun --------------" << endl;
  
  int i;
  hist.clear();


  for (i=0; i<m_nodes.isize(); i++) {
    m_nodes[i].SetUsed(false);
  }


  
  svec<int> ants;

  ants.resize(antnum, -1);
  
  svec<int> goodFirst;
  goodFirst.resize(m_nodes.isize(), 1);
  for (i=0; i<ants.isize(); i++) {
    ants[i] = m_init.EdgeNode(Pick(m_init.Phero(), goodFirst));
    goodFirst[ants[i]] = 0;
    m_nodes[ants[i]].SetActive(true);
    //cout << " Init " << i << " " <<  ants[i] << endl;
  }

  int timestamp = 0;
  for (i=0; i<m_nodes.isize(); i++) {
    if (m_nodes[i].GetManip() != NULL) {
      m_nodes[i].GetManip()->OnCycle(timestamp, i, m_nodes[i]);
    }
  }
  timestamp++;
  

  while (true) { // Done!!
    int done = 0;
    for (i=0; i<ants.isize(); i++) {
      if (ants[i] == -1) {
	done++;
      } else {
	//cout << "Cycle" << endl;
	int a = OneCycle(ants[i], hist, i);
	ants[i] = a;
	if (a == -1)
	  done++;
      }
    }
    //if (done > 0)
    //cout << "Done " << done << endl;
    //++++++++++++++++++++++++++++++++ Optional node check ++++++++++++++++++++++++++++++++++
    if (done == ants.isize() || m_types.Complete(hist, m_nodes)) {
      //cout << "Yes, done" << endl;
      break;
    }


    for (i=0; i<m_nodes.isize(); i++) {
      if (m_nodes[i].GetManip() != NULL) {
	m_nodes[i].GetManip()->OnCycle(timestamp, i, m_nodes[i]);
      }
    }
    timestamp++;

    

  }

  //===================================================
  if (!m_types.Complete(hist, m_nodes))
    return inf_score;
    
  //cout << "OneRun done, time: " << hist.TotalTime() << endl;
  if (m_bCost || m_bRisk) {
    if (m_bCost) {
      if (m_lastCost)
	return hist.LastCost();
      else
	return hist.TotalCost();
    } else {
      return hist.TotalRisk();
    }
  } else {
    return hist.LongestTime();
  }

}
  
int AntsGraph::OneCycle(int ant, AHistory & hist, int curr) 
{
  m_nodes[ant].Inc();

  //cout << "cycle " << m_nodes[ant].Cycle() << " " << m_nodes[ant].Duration() << endl;
  if (m_nodes[ant].Cycle() > m_nodes[ant].Duration()) {
    cout << "NOOO " << m_nodes[ant].Cycle() << " " <<  m_nodes[ant].Duration() << endl;
    throw;
  }
  
  if (m_nodes[ant].Cycle() == m_nodes[ant].Duration()) {
    //cout << "ON" << endl;
    return Switch(ant, hist, curr);    
  }

  return ant;
}

int AntsGraph::Switch(int ant, AHistory & hist, int curr) 
{
  int i;
  svec<int> good, goodnodes;
  goodnodes.resize(m_nodes.isize(), 1);
  good.resize(m_nodes[ant].EdgeCount(), 1);

  int bad = 0;

  //cout << "Switch " << ant << endl;
  int cand = 0;
  for (i=0; i<hist.isize(); i++) {

    // If optional, allow multiple instances
    int type = m_nodes[hist[i]].Type();
    if (type >= 0 && m_types.IsOptional(type)) {
      cout << "SCREAMM!!!" << endl;
      continue;
    }

   
    if (goodnodes[i] == 1)
      bad++;
    goodnodes[i] = 0;
    
    int idx = m_nodes[ant].GetEdgeFor(hist[i]);
    if (idx >= 0) {
      //cout << "Set bad (1), ant: " << ant << " node: " << hist[i] << " edge " << idx << endl;
      good[idx] = 0;          
    }
  }
  
  for (i=0; i<m_nodes.isize(); i++) {
    if (m_nodes[i].IsActive()) {
      if (goodnodes[i] == 1)
	bad++;
      goodnodes[i] = 0;
    
      int idx = m_nodes[ant].GetEdgeFor(i);
      //cout << "Node " << ant << " idx: " 
      if (idx >= 0) {      
	good[idx] = 0;
	//cout << "Set bad (2), node " << i << " edge " << idx << endl;
      }
    }
  }

  double risk = 1. - (1. - m_nodes[ant].Risk())*(1. - m_nodes[ant].EdgeRisk());

  // DEBUG!!!!!!!!!!!!!!!!!!!!!!

  /*
  cout << "BAD: " << bad << " of " << goodnodes.isize() << endl;

  for (i=0; i<goodnodes.isize(); i++)
    cout << " status: " << i << " = " << goodnodes[i] << endl;

  for (i=0; i<good.isize(); i++) {
    cout << " edge status: " << i << " -> " << good[i] << endl;
    }*/
  
  
  if (bad >= goodnodes.isize()) {    
    //cout << "Cycle done, cycles: " << m_nodes[ant].TotalCycles() << endl;
    //cout << "PUSH final node: " << ant << "  " << m_nodes[ant].Label() << endl;
  
    // WARNING: Should take the cost per edge!!!
    //cout << "PUSH " << ant << endl;
    hist.push_back(ant, m_nodes[ant].TotalCycles(), m_nodes[ant].EdgeCost() + m_nodes[ant].Cost(), risk, curr, m_nodes[ant].Label());
    hist.push_back_edge(-1);
    m_nodes[ant].ResetSoft();
    m_nodes[ant].SetUsed(true);
    return -1;
  }
  
  int last = ant;

  //cout << "PUSH node: " << ant << " " << m_nodes[ant].Label() << endl;
  //cout << "PUSH (2) " << ant << endl;
  hist.push_back(ant, m_nodes[ant].TotalCycles(), m_nodes[ant].EdgeCost() + m_nodes[ant].Cost(), risk, curr, m_nodes[ant].Label());
  // cout << "cycles spent: " << m_nodes[ant].TotalCycles() << " -> " << ant << " " << bad << endl;
  m_nodes[ant].ResetSoft();
  m_nodes[ant].SetUsed(true);

  //good[ant] = 0;
  int edge = Pick(m_nodes[ant].Phero(), good);
  if (edge == -1) {
    hist.push_back_edge(-1);
    //cout << "OUT of edges." << endl;
    return -1;
  }

  
  //-----------------------------------------------------
  // ---------------- FIX THIS --------------------------
  // Current code assumes edges.isize() == # of nodes!!!

  //cout << "DEBUG " << m_nodes[last].EdgeCount() << " " << edge << endl;
  ant = m_nodes[last].Index(edge);
  //cout << "PICKED: " << ant << " edge " << edge << " last " << last << endl;
  //hist.Print();
  
  m_nodes[ant].Cycle() -= m_nodes[last].Edge(edge).Duration(); // Add in duration
  m_nodes[ant].EdgeCost() = m_nodes[last].Edge(edge).Cost(); // Add edge cost
  m_nodes[ant].EdgeRisk() = m_nodes[last].Edge(edge).Risk(); // Add edge cost

  hist.push_back_edge(edge);
  //cout << m_nodes[ant].IsActive() << endl;
  m_nodes[ant].SetActive(true);
  //cout << "Pick next task " << ant << " for ant " << curr << endl;
  return ant;
  
}

int AntsGraph::Pick(const svec<double> & phero, const svec<int> & good)
{
  int i;

  if (m_forced.isize() > 0) {
    m_forceCount++;
    return m_forced[m_forceCount - 1];
  }

  if (good.isize() == 0)
    return -1;
  
  svec<double> diff;
  double sum = 0.;
  //cout << "Start PICK" << endl;
  int gc = 0;
  for (i=0; i<phero.isize(); i++) {
    //cout << "PHERO Loop: " << i << " " << good[i] << endl;
    
    double d = phero[i];
    if (good[i] == 0)
      d = 0.;
    else
      gc++;
    diff.push_back(sum + d);
    sum += d;
  }

  //cout << "Good count: " << gc << endl;
  if (gc == 0)
    return -1;
  
  double n = RandomFloat(sum);
  //cout << n << endl;
  
  for (i=diff.isize()-2; i>=0; i--) {
    //cout << i << " -> " << diff[i] << " " << good[i] << endl;
    if (good[i+1] == 1 && diff[i] < n)
      break;
  }

  
  //cout << "Picking " << i+1 << endl;
  //if (i+1 >=0)
  //cout  << m_nodes[i+1].Label() << endl;
  
  return i+1;
}


void AntsGraph::Read(const string & fileName)
{
  
  FlatFileParser parser;
  
  parser.Open(fileName);
  int i, j;

  while (parser.ParseLine()) {
    if (parser.GetItemCount() < 2)
      continue;

    if (parser.AsString(0) == "nodes") {
      m_nodes.resize(parser.AsInt(1));

      for (i=0; i<m_nodes.isize(); i++)
	m_init.AddEdge(i, 0, 0, 0);

      continue;
    }
    
    if (parser.AsString(0) == "node") {
      int idx = parser.AsInt(1);
      int dur = parser.AsInt(3);
      double cost = parser.AsFloat(5);
      double risk = parser.AsFloat(7);

      m_nodes[idx].Duration() = dur;
      m_nodes[idx].Cost() = cost;
      m_nodes[idx].Risk() = risk;
      
      for (i=0; i<m_nodes.isize(); i++) {
	parser.ParseLine();
	if (parser.GetItemCount() < 2)
	  break;

	m_nodes[idx].AddEdge(parser.AsInt(1), parser.AsInt(3), parser.AsFloat(5), parser.AsFloat(7));      
	
      }
    }
  }
}
