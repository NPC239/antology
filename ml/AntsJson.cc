#include "ml/AntsJson.h"
#include "base/StringUtil.h"
#include <iostream>
#include <fstream>
#include <sstream>      // std::stringstream

void AntsJson::ReadFile(AntsGraph & a, const string & fileName)
{
  std::ifstream in(fileName); 
  in >> m_js;

  Construct(a);
}

void AntsJson::ReadData(AntsGraph & a, const string & data)
{
  stringstream ss;
  ss << data << endl;

  ss >> m_js;

  Construct(a);

}
  
void AntsJson::WriteFile(AntsGraph & a, const string & fileName)
{
  Update(a);

  std::ofstream out(fileName); 
  out << m_js;

}

void AntsJson::WriteData(AntsGraph & a, string & data)
{
  Update(a);

  stringstream ss;
  ss << m_js << endl;

  ss >> data;

}

void AntsJson::Update(AntsGraph & a)
{
  int i, j;

  // History first
  const AHistory & h = a.GetCurrentBestHistory();

  m_js["path"]["total_time"] = h.TotalTime();
  m_js["path"]["total_cost"] = h.TotalCost();
  m_js["path"]["total_risk"] = h.TotalRisk();

  string p = "{ \"best_path\": [";
  for (i=0; i<h.isize(); i++) {
    p += "{";
    p += "\"index\": " + Stringify(i) + ","; 
    p += "\"node\": " + Stringify(h[i]) + ","; 
    p += "\"cost\": " + Stringify(h.Cost(i)) + ","; 
    p += "\"time\": " + Stringify(h.Time(i)) + ","; 
    p += "\"risk\": " + Stringify(h.Risk(i)) + ","; 
    p += "\"name\": \"" + h.Label(i) + "\"";
    p += "}";
    if (i < h.isize()-1)
      p += ",";
   
  }

  p += "]}\n";

  cout << "STRING: " << p << endl;
  
  nlohmann::json path = nlohmann::json::parse(p);

  m_js.merge_patch(path);
}

void AntsJson::Construct(AntsGraph & a)
{
  int i;

  // string optim = m_js["optimize"].value();

 // Types

  if (m_js.contains("types")) {
    for (nlohmann::json::iterator it4 = m_js["types"].begin(); it4 != m_js["types"].end(); ++it4) {
      //std::cout << *it2 << "\n";
      const auto xy = *it4;

      
      string name = xy["name"];
      string property = xy["property"];
      bool bOpt = false;
      if (property == "optional")
	bOpt = true;
      
      a.Types().AddType(bOpt, name);
    }
  }

  cout << "Types, done" << endl;


  
  for (nlohmann::json::iterator it1 = m_js.begin(); it1 != m_js.end(); ++it1) {
    //std::cout << *it << "\n";
    const auto xy = *it1;
    if (it1.key() == "optimize") {
      if (it1.value() == "cost") {
	a.SetOptimizeCost(true);
      }
     if (it1.value() == "risk") {
	a.SetOptimizeRisk(true);
      }
    }
  }
  cout << "Labels, done" << endl;

  a.resize(m_js["nodes"].size());
  
  // Nodes
  int index = 0;
  for (nlohmann::json::iterator it2 = m_js["nodes"].begin(); it2 != m_js["nodes"].end(); ++it2) {
    //std::cout << *it2 << "\n";
    auto xy = *it2;

    ANode n;
      
    //int index = xy["index"];
    n.Label() = xy["name"];
    if (xy.contains("type")) {
      //cout << "TYPE is set!" << endl;
      string type = xy["type"];
      n.Type() = a.Types().NameIndex(type);
    }

    //cout << "What" << endl;
    
    for (nlohmann::json::iterator it15 = xy["attributes"].begin(); it15 != xy["attributes"].end(); ++it15) {
      const auto zz = *it15;
      //cout << "Try" << endl;
      if (zz.contains("duration"))
	n.Duration() = zz["duration"];
      if (zz.contains("cost"))
	n.Cost() = zz["cost"];
      if (zz.contains("risk"))
	n.Risk() = zz["risk"];
      if (zz.contains("code"))
	n.Risk() = zz["code"];
    }
    
    a[index] = n;
    a.AddInit(index);
    index++;
  }
  cout << "Nodes, done" << endl;

  // cout << "Edges" << endl;
  // Edges
  for (nlohmann::json::iterator it3 = m_js["edges"].begin(); it3 != m_js["edges"].end(); ++it3) {
    //std::cout << *it2 << "\n";
    auto xy = *it3;

      
    string name = xy["name"];
    string from = xy["from"];
    string to = xy["to"];
    int duration = 1;
    double cost = 0.;
    double risk = 0.;

    for (nlohmann::json::iterator it15 = xy["attributes"].begin(); it15 != xy["attributes"].end(); ++it15) {
      const auto zz = *it15;
      if (zz.contains("duration"))
	duration = zz["duration"];
      if (zz.contains("cost"))
	cost = zz["cost"];
      if (zz.contains("risk"))
	risk = zz["risk"];
      if (zz.contains("code"))
	risk = zz["code"];
    }

    
    a[a.FindNode(from)].AddEdge(a.FindNode(to), duration, cost, risk);
  }
  cout << "Edges, done" << endl;


  a.Print();
}
 
