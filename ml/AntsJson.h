#ifndef ANTSJSON_H
#define ANTSJSON_H

#include "extern/json/nlohmann/json.hpp"
#include "base/SVector.h"
#include "ml/AntsGraph.h"

class AntsJson
{
 public:
  AntsJson() {}


  void ReadFile(AntsGraph & a, const string & fileName);
  void ReadData(AntsGraph & a, const string & data);

  void WriteFile(AntsGraph & a, const string & fileName);
  void WriteData(AntsGraph & a, string & data);
  
  
 private:
  void Construct(AntsGraph & a);
  void Update(AntsGraph & a);
  
  nlohmann::json m_js;
};



#endif //ANTSJSON_H



