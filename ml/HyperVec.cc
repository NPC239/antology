#define FORCE_DEBUG

#include "ml/HyperVec.h"
#include "base/RandomStuff.h"
#include "math/Matrix.h"


void HyperVector::Read(IMReadStream &s)
{
  s.Read(m_hits);
  int n;
  s.Read(n);
  resize(n);
  for (int i=0; i<n; i++)
    s.Read(m_data[i]);
}

void HyperVector::Write(IMWriteStream &s) const
{
  s.Write(m_hits);
  s.Write(m_data.isize());
  for (int i=0; i<m_data.isize(); i++)
    s.Write(m_data[i]);
}


HyperVector::HyperVector()
{
  resize(8192);
  //resize(32);
  RandomInit();
  m_hits = 0.;
}

void HyperVector::RandomInit()
{
  int i;
  for (i=0; i<m_data.isize(); i++) {
    if (RandomFloat(1.) < 0.5)
      m_data[i] = 1;
    else
      m_data[i] = -1;
  }
}


void HyperVector::Fold(int n)
{
  int i;
  
  for (i=0; i<m_data.isize(); i++) {
    m_data[i] = m_data[i % n];
  }
}

void HyperVector::UnFold(int n)
{
  int i;
  MajorityVote vote;
  int m = m_data.isize()/n;

  for (i=0; i<n; i++) {
    Rotate(i*m);
    //cout << "Rotate by " << i*m << endl;
    vote.Add(*this);
  }
  *this = vote.Vote();
}

double HyperVector::Dot(const HyperVector & h, int from, int to) const
{
  double s1 = 0.;
  double s2 = 0.;
  int i;
 
  for (i=from; i<to; i++) {
    s1 += (double)m_data[i]*(double)m_data[i];
    s2 += (double)h[i]*(double)h[i];
  }
  s1 = sqrt(s1);
  s2 = sqrt(s2);

  double dot = 0.;
  for (i=from; i<to; i++) {
    dot += (double)m_data[i]*(double)h[i];
  }

  dot /= s1;
  dot /= s2;
  
  return dot;
}

double HyperVector::operator * (const HyperVector & h) const
{
  double s1 = 0.;
  double s2 = 0.;
  int i;
  for (i=0; i<isize(); i++) {
    s1 += (double)m_data[i]*(double)m_data[i];
    s2 += (double)h[i]*(double)h[i];
  }
  s1 = sqrt(s1);
  s2 = sqrt(s2);

  double dot = 0.;
  for (i=0; i<isize(); i++) {
    dot += (double)m_data[i]*(double)h[i];
  }

  dot /= s1;
  dot /= s2;
  
  return dot;
}

void HyperVector::Rotate(int n)
{
  int i, j;
  //cout << "HyperVector::Rotate by " << n << endl;
  HyperVector tmp = *this;
  for (i=0; i<isize(); i++) {
    int index = (i-n)%isize();
    if (index < 0)
      index += isize();
    //cout << index << " -> " << i << endl;
    tmp[i] = m_data[index];
  }
  *this = tmp;

  
  /*
  if (n < 0)
    n += m_data.isize();

  double tmp = m_data[0];
  for (j=0; j<n; j++) {
    int last = j;
    for (i=0; i<m_data.isize(); i+= n) {
      int idx = (i+j+n) % m_data.isize();
      double tmp1 = m_data[idx];
      m_data[idx] = tmp;
      tmp = tmp1;
      //cout << last << " -> " << idx << endl;
      last = idx;
    }
    }*/

}

void HyperVector::Print() const
{
  int i;
  //int n = 0;
  int plus = 0;
  int minus = 0;
  for (i=0; i<m_data.isize(); i++) {
    if (m_data[i] > 0)
      cout << " ";
    cout << " " << (int)m_data[i];
    if (m_data[i] == 1)
      plus++;
    if (m_data[i] == -1)
      minus++;
    
  }
  cout << endl;
  cout << "Plus: " << plus << " Minus: " << minus << endl;
}
void HyperVector::Print(const HyperVector & h) const
{
  int i;
  
  int n = 0;
  for (i=0; i<m_data.isize(); i++) {
    cout << "  ";
    if (h[i] == m_data[i]) {
      cout << " ";
    } else {
      cout << "*";
      n++;
    }
  }
  cout << endl;
  for (i=0; i<m_data.isize(); i++) {
    if (m_data[i] > 0)
      cout << " ";
    cout << " " << (int)m_data[i];
    
  }
  cout << endl;
  for (i=0; i<m_data.isize(); i++) {
    if (h[i] > 0)
      cout << " ";
    cout << " " << (int)h[i];
    
  }
  cout << endl << "Mismatches: " << n << endl;
}



HyperVector MajorityVote::Vote() const
{
  return Vote(m_vec, m_weight);
}


HyperVector MajorityVote::Vote(const svec<HyperVector> & v, const svec<double> & weight) const
{
  int i, j;
  HyperVector out;
  out.resize(v[0].isize(), 0.);

  for (i=0; i<out.isize(); i++) {
    double one = 0;
    double zero = 0;
    for (j=0; j<v.isize(); j++) {
      if ((v[j])[i] < 0)
	zero += weight[j];
      else
	one += weight[j];
    }
    if (zero == one) {
      if (RandomFloat(1) > 0.5)
	out[i] = 1;
      else
	out[i] = -1;
    } else {
      if (one > zero)
	out[i] = 1;
      else
	out[i] = -1;
      
    }
  }
  
  return out;
}

void HyperVecPool::Read(IMReadStream &s)
{
  int i;
  
  int n = 0;
  s.Read(n);
  m_index.resize(n);
  m_data.resize(n);
  
  for (i=0; i<n; i++)
    s.Read(m_index[i]);

  for (i=0; i<n; i++)
    m_data[i].Read(s);

  m_null.Read(s);
  
  s.Read(n);
  m_dim.resize(n);
  for (i=0; i<n; i++)
    m_dim[i].Read(s);
    

}

void HyperVecPool::Write(IMWriteStream &s) const
{
  int i;
  s.Write(m_index.isize());
  
  for (i=0; i<m_index.isize(); i++)
    s.Write(m_index[i]);

  for (i=0; i<m_data.isize(); i++)
    m_data[i].Write(s);

  m_null.Write(s);
  
  s.Write(m_dim.isize());
 
  for (i=0; i<m_dim.isize(); i++)
    m_dim[i].Write(s);
}

void HyperVecPool::Read(const string & fileName)
{
  CMReadFileStream s;
  s.Open(fileName.c_str());
  Read(s);
  s.Close();
}

void HyperVecPool::Write(const string & fileName) const
{
  CMWriteFileStream s;
  s.Open(fileName.c_str());
  Write(s);
  s.Close();
}

void HyperVecPool::MakeDistMatrix(Matrix & m) const
{
  m.SetSize(m_data.isize(), m_data.isize());
  int i, j;

  for (i=0; i<m_data.isize(); i++) {
    for (j=0; j<m_data.isize(); j++) {
      double v = m_data[i].SimpleDot(m_data[j]);
      double d = 1. - (v+1)/2;
      m.Set(i, j, d);
    }
  }
}

void HyperVecPool::ReIndex(const svec<int> & idx)
{
  svec<int> tmp;
  tmp = idx;
  for (int i=0; i<idx.isize(); i++) {
    tmp[i] = m_index[idx[i]];
  }
  m_index = tmp;
}


//==============================================================

void HyperSet::Setup(int n, double warp, int spacing, double low, double high)
{
  SetRange(low, high);
  
  int i, j;
  
  m_zero.RandomInit();
  m_one.RandomInit();
  
  m_vec.resize(n+1);

  
  for (i=0; i<=n; i++) {
    m_vec[i] = m_zero;
    m_vec[i].MergeRand(m_one, (double)i/(double)n);
  }

  
  double rate = warp;
  for (i=0; i<m_vec.isize(); i+=spacing) {
    HyperVector rnd;
    rnd.RandomInit();
    for (j=0; j<m_vec.isize(); j++) {
      double w = rate*(1-(double)(i-j)/(double)n);
      if (w < 0)
	w = -w;
      //cout << i << " " << j << " w=" << w << endl;
      m_vec[i].MergeRand(rnd, w);
    }

  }
}

void HyperSet::Print() const
{
  for (int i=0; i<m_vec.isize(); i++) {  
    for (int j=0; j<m_vec.isize(); j++) {
      cout << " " << m_vec[i]*m_vec[j];
    }
    cout << endl;    
  }
}

HyperVector RandomMerge::Merge(const HyperVector & one, const HyperVector & two, double w) const
{
  HyperVector out = one;
 
  int i;
  int n = w*m_index.isize();
  if (n > m_index.isize())
    n = m_index.isize();
  for (i=0; i<n; i++) {
    out[m_index[i]] = two[m_index[i]];
  }
  return out;
}
 
//=============================================
void HyperVecModel::SetSize(int dim, int n)
{
  //cout << dim << " " << n << endl;
  
  int i;
  if (n < 0) {
    HyperVector tmp;
    m_n = n = tmp.isize();
  } else {
    m_n = n;
  }

  //cout << n << endl;
  m_key.resize(n);
  //m_key.Print();
  
  m_key.RandomInit();
  m_rm.resize(n);
  m_bottom.resize(n);
  m_top.resize(n);

  
  for (i=0; i<dim; i++) {
    m_rm[i].resize(n);
    m_rm[i].Init();
    m_bottom[i].resize(n);
    m_top[i].resize(n);
    m_bottom[i].RandomInit();
    m_top[i].RandomInit();
    
    //m_bottom[i].Print();
    //m_top[i].Print();
  }  
}

void HyperVecModel::Add(const svec<double> & data)
{
  int i, j;
  HyperVector cum = m_key;
  for (j=0; j<data.isize(); j++) {
    HyperVector a = m_rm[j].Merge(m_bottom[j], m_top[j], data[j]);
    cum.Multiply(a);
  }
  m_vote.Add(cum);    
}

void HyperVecModel::Done()
{
  m_model = m_vote.Vote();
  m_vote.Clear();
}


double HyperVecModel::Cosine(const svec<double> & data) const
{
  int i, j;
  HyperVector cum = m_key;
  for (j=0; j<data.isize(); j++) {
    HyperVector a = m_rm[j].Merge(m_bottom[j], m_top[j], data[j]);
    cum.Multiply(a);
  }
  return m_model * cum;    
}


//=====================================================
void MetaHVModel::Add(const svec<double> & data)
{
  HVDataPoint p;
  p.Data() = data;
  m_data.push_back(p);
}

void MetaHVModel::Train(int max)
{
  int i, j;

  // First model
  m_models.resize(1);
  m_models[0].SetSize(m_dim, m_n);

  for (i=0; i<m_data.isize(); i++) {
    m_models[0].Add(m_data[i].Data());
  }

  m_models[0].Done();
  m_models[0].Hits() = m_data.isize();
  
  // Split models

  
  for (j=0; j<max-1; j++) {
    double thresh;
    int index = HighestError(thresh);
    cout << "Split model " << index << " thresh: " << thresh << " hits: " << m_models[index].Hits() << endl;
    if (index == -1)
      break;
    
    HyperVecModel split;
    split.SetSize(m_dim, m_n);
    int nn = m_models.isize();
    m_models.push_back(split);

    // Re-assign the model id
    int moved = 0;
    for (i=0; i<m_data.isize(); i++) {
      if (m_data[i].Model() == index) {
	if (m_models[index].Cosine(m_data[i].Data()) < thresh) {
	  m_data[i].Model() = nn;
	  moved++;
	  //cout << "Move " << i << " to model " << nn << " " << m_models[index].Hits() << endl;
	} else {
	  //cout << "Keep " << i << " in model " << index << " " << m_models[index].Hits() << endl;
	}
      }
    }

    // Undo if not enough data points available
    if (moved < m_minPer || m_models[index].Hits() - moved < m_minPer) {
      m_models[nn].Hits() = 0;
      //cout << "UNDO " << moved << " " << m_models[index].Hits() - moved << endl;
      for (i=0; i<m_data.isize(); i++) {
	if (m_data[i].Model() == nn) {
	  m_data[i].Model() = index;
	}
      }
    } else {       
      m_models[nn].Hits() = moved;
      m_models[index].Hits() -= moved;
    }
   
    
    for (i=0; i<m_data.isize(); i++) {
      m_models[m_data[i].Model()].Add(m_data[i].Data());
    }
    for (i=0; i<m_models.isize(); i++) {
      m_models[i].Done();
    }   
  }  
}

int MetaHVModel::HighestError(double & thresh) const
{
  int i, j;
  double min = 1.1;
  int index = -1;
  
  for (i=0; i<m_models.isize(); i++) {
    int c = 0;
    double d = 0.;
    svec<double> scores;
    
    for (j=0; j<m_data.isize(); j++) {
      if (m_data[j].Model() == i) {
	c++;
	double dd = m_models[i].Cosine(m_data[j].Data());
	d += dd;
	scores.push_back(dd);
      }
    }
    if (c >= 2*m_minPer) {
      d /= (double)c;
      if (d < min) {
	min = d;
	index = i;
	Sort(scores);
	thresh = scores[scores.isize()/2];
      }
    }
  }
  return index;
}

double MetaHVModel::BestCosine(const svec<double> & data) const
{
  int index;
  return BestCosine(index, data);
}

double MetaHVModel::BestCosine(int & model, const svec<double> & data) const
{
  int i;
  model = -1;
  double max = 0.;

  for (i=0; i<m_models.isize(); i++) {
    if (m_models[i].Hits() < m_minPer)
      continue;
    double d = m_models[i].Cosine(data);
    if (d > max) {
      max = d;
      model = i;
    }
  }
  return max;
}
