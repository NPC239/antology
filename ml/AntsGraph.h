#ifndef ANTSGRAPH_H
#define ANTSGRAPH_H


#include "base/SVector.h"
#include "plugins/LuaWrap.h"


const double inf_score = 99999999.;

class IAntsManip;

class AHistory
{
 public:
  AHistory() {
    m_ants = 0;
  }

  int isize() const {return m_past.isize();}
  int & operator[] (int i) {return m_past[i];}
  const int & operator[] (int i) const {return m_past[i];}
  void resize(int n, double d = 0.) {
    m_past.resize(n, d);
    m_time.resize(n, 0);
    m_cost.resize(n, 0.);
    m_risk.resize(n, 0.);
    m_ant.resize(n, -1);
    m_label.resize(n);
  }

  void clear() {
    m_past.clear();
    m_time.clear();
    m_cost.clear();
    m_risk.clear();
    m_ant.clear();
    m_label.clear();
  }
  
  void push_back(int idx, int time, double cost, double risk, int ant, const string & label) {
    m_past.push_back(idx);
    m_time.push_back(time);
    m_cost.push_back(cost);
    m_risk.push_back(risk);
    m_ant.push_back(ant);
    m_label.push_back(label);
    if (ant > m_ants)
      m_ants = ant;
  }
  void push_back_edge(int edge) {
    m_edge.push_back(edge);
  }

  int TotalTime() const {
    int i;
    int sum = 0;

    for (i=0; i<m_time.isize(); i++)
      sum += m_time[i];
    return sum;
  }
  
  double LastCost() const {
    return m_cost[m_cost.isize()-1];
  }
  
  double TotalCost() const {
    double sum = 0.;
    for (int i=0; i<m_cost.isize(); i++)
      sum += m_cost[i];
    return sum;
  }

  double SumRisk() const {
    double sum = 0.;
    for (int i=0; i<m_risk.isize(); i++)
      sum += m_risk[i];
    return sum;
    
  }
  
  double TotalRisk() const {
    double sum = 1.;
    for (int i=0; i<m_risk.isize(); i++)
      sum *= (1. - m_risk[i]);
    return 1. - sum;
  }
  
  int LongestTime() const {
    int i, j;
    int max = 0;
    
    for (int i=0; i<=m_ants; i++) {
      int sum = 0;
      for (j=0; j<m_ant.isize(); j++) {
	if (m_ant[j] == i) {
	  sum += Time(j);
	}
      }
      if (sum > max)
	max = sum;
    }
    return max;
  }
  
  int & Time(int i) {return m_time[i];}
  const int & Time(int i) const {return m_time[i];}

  int & Ant(int i) {return m_ant[i];}
  const int & Ant(int i) const {return m_ant[i];}

  double & Cost(int i) {return m_cost[i];}
  const double & Cost(int i) const {return m_cost[i];}

  double & Risk(int i) {return m_risk[i];}
  const double & Risk(int i) const {return m_risk[i];}

  string & Label(int i) {return m_label[i];}
  const string & Label(int i) const {return m_label[i];}

  int EdgeCount() const {return m_edge.isize();}
  // Can be -1!
  int & Edge(int i) {return m_edge[i];}
  const int & Edge(int i) const {return m_edge[i];}

  void Print() const {
    for (int j=0; j<=m_ants; j++) {
      int t = 0;
      double c = 0.;
      double r = 1.;
      for (int i=0; i<m_past.isize(); i++) {
	if (m_ant[i] == j) {
	  cout << "Ant: " << m_ant[i] << " task: " << m_past[i] << " time " << m_time[i] << " cost " << m_cost[i] << " risk " << m_risk[i] << " \t" << m_label[i] << endl;
	  t += m_time[i];
	  c += m_cost[i];
	  r *= (1. - m_risk[i]);
	}
      }
      cout << "Ant time: " << t << endl;
      cout << "Ant cost: " << c << endl;
      cout << "Ant risk: " << 1. - r << endl;
    }

  }
  
 private:
  svec<int> m_past;
  svec<int> m_ant;
  svec<int> m_time;
  svec<string> m_label;
  svec<double> m_cost;
  svec<double> m_risk;
  svec<int> m_edge;
  int m_ants;
};



class AEdge
{
 public:
  AEdge() {
    m_duration = 0;
    m_cost = 0.;
    m_risk = 0.;
    m_node = -1;
  }
  
  int & Duration() {return m_duration;}
  double & Cost() {return m_cost;}
  double & Risk() {return m_risk;}
  
  const int & Duration() const {return m_duration;}
  const double & Cost() const {return m_cost;}
  const double & Risk() const {return m_risk;}

  string & Code() {return m_code;}
  const string & Code() const {return m_code;}

 private:
  int m_duration;
  int m_node;
  double m_cost;
  double m_risk;
  string m_code;
};



class ANode
{
 public:
  ANode() {
    Reset();
    m_pManip = NULL;
  }

  void Reset() {
    m_active = false;
    m_cycle = 0;
    m_duration = 1;
    m_cost = 0.;
    m_totalCycles = 0;
    m_risk = 0.;
    m_edgecost = 0.;
    m_edgerisk = 0.;
    m_type = -1;
    m_edgeEntry = -1;
    m_used = false;
  }

  void ResetSoft() {
    m_active = false;
    m_cycle = 0;
    m_totalCycles = 0;
    m_edgecost = 0.;
    m_edgerisk = 0.;
    m_edgeEntry = -1;
  }

  void SetUsed(bool b) {
    m_used = b;
  }

  void ResetPhero(double keep_weight = 0.) {
    // Set it to 1.
    for (int i=0; i<m_phero.isize(); i++)
      m_phero[i] = keep_weight * m_phero[i] + (1 - m_phero[i]);
  }


  IAntsManip * GetManip() {return m_pManip;}
  void SetManip(IAntsManip * p) {
    m_pManip = p;
  }
  
  
  bool IsActive() const {return m_active;}
  void SetActive(bool b) {
    m_active = b;   
  }

  string & Code() {return m_code;}
  const string & Code() const {return m_code;}

  int & Cycle() {return m_cycle;}
  int & TotalCycles() {return m_totalCycles;}
  int & Duration() {return m_duration;}
  double & Cost() {return m_cost;}
  double & Risk() {return m_risk;}

  double & EdgeCost() {return m_edgecost;}
  const double & EdgeCost() const {return m_edgecost;}
  double & EdgeRisk() {return m_edgerisk;}
  const double & EdgeRisk() const {return m_edgerisk;}
  int & Entry() {return m_edgeEntry;}
  const int & Entry() const {return m_edgeEntry;}

  int & Type() {return m_type;}
  const int & Type() const {return m_type;}
  
  
  void Inc() {
    m_cycle++;
    m_totalCycles++;
  }

  const int & Cycle() const {return m_cycle;}
  const int & TotalCycles() const {return m_totalCycles;}
  const int & Duration() const {return m_duration;}
  const double & Cost() const {return m_cost;}
  const double & Risk() const {return m_risk;}

  void AddEdge(int index, int duration, double cost, double risk) {
    AEdge a;
    a.Duration() = duration;
    a.Cost() = cost;
    a.Risk() = risk;
    m_edges.push_back(a);
    m_phero.push_back(1);
    m_index.push_back(index);
  }

  // Inefficient!!!!
  int GetEdgeFor(int node) const {
    for (int i=0; i<m_index.isize(); i++) {
      if (m_index[i] == node)
	return i;
    }
    return -1;
  }

  int EdgeNode(int i) const {
    return m_index[i]; 
  }
  
  int EdgeCount() const {return m_edges.isize();}
  const AEdge & Edge(int i) const {return m_edges[i];}
  AEdge & Edge(int i) {return m_edges[i];}

  const svec<double> & Phero() const {return m_phero;}
  const svec<int> & Index() const {return m_index;}

  svec<double> & Phero() {return m_phero;}
  svec<int> & Index() {return m_index;}

  int Index(int i) const {return m_index[i];}
  
  void Decay();
  void Learn(int idx, double diff);

  string & Label() {return m_label;}
  const string & Label() const {return m_label;}

  // Returns the 
  int Write(svec<double> & in, svec<double> & out) const {
    // element 0 is the index (extern)
    // 1 timestamp (extern)
    // 2 active
    // 3 cycle
    // 4 duration
    // 5 cost
    // 6 risk
    
    in.resize(8, 0.);
    if (m_active)
      in[2] = 1.;
    else
      in[2] = 0.;
    in[3] = m_cycle;
    in[4] = m_duration;
    in[5] = m_cost;
    in[6] = m_risk;
    if (m_used)
      in[7] = 1.;
    else
      in[7] = 0.;

    out.resize(4, 0);
    out[1] = m_duration;
    out[2] = m_cost;
    out[3] = m_risk;
    
    return 4;
  }
  
  void Read(const svec<double> & buf) {
    // 0 has changed    
    // 1 duration (DISABLED!)
    // 2 cost
    // 3 risk
    if (buf[0] > 0) {
      m_cost = buf[2];
      m_risk = buf[3];
    }
  }

  
 private:
  int m_type;
  int m_edgeEntry;
  
  int m_cycle;
  int m_totalCycles;
  bool m_active;
  bool m_used;
  
  double m_edgecost;
  double m_edgerisk;
  
  int m_duration;
  double m_cost;
  double m_risk;
  
  svec<double> m_phero;
  svec<int> m_index;

  svec<AEdge> m_edges;

  IAntsManip * m_pManip;
  string m_label;

  string m_code;

  
};



class TypeMgr
{
public:
  TypeMgr() {
    m_total = 0;
  }

  bool IsOptional(int i) const {
    if (m_required[i] == 0)
      return true;
    return false;
  }
  
  int AddType(bool optional, const string & name = "") {
    if (optional) {
      m_required.push_back(0);
    } else {
      m_required.push_back(1);
      m_total++;
    }
    m_name.push_back(name);
    return m_required.isize()-1;
  }

  int NameIndex(const string & name) const {
    for (int i=0; i<m_name.isize(); i++) {
      if (name == m_name[i])
	return i;
    }
    return -1;
  }

  bool Complete(const AHistory & h, const svec<ANode> & nodes) const {
    int i;
    int count = 0;
    int valid = 0;

    //===================================================
    // INEFFICIENT!!!
    int implicit = 0;
    // Pre-compute this!!!
    for (i=0; i<nodes.isize(); i++) {
      if (nodes[i].Type() == -1)
	implicit++;
    }

    int imp_found = 0;
    
    for (i=0; i<h.isize(); i++) {
      int n = h[i];
      if (nodes[n].Type() == -1) {
	imp_found++;	
	continue;
      }
      
      valid++;
      if (m_required[nodes[n].Type()] == 1) {
	count++;
      }
    }
    //cout << "COMPLETE: size=" << h.isize() << " " << valid << " " << count << " " << m_total << " " << implicit << " " << imp_found << endl;
    //cout << "LAST found: " << h[h.isize()-1] << endl;
    if (count == m_total && imp_found == implicit) {
      //cout << "TRUE" << endl;
      return true;
    }
    return false;
    
  }
private:
  int m_total;
  svec<int> m_required;
  svec<string> m_name;
};



class AntsGraph
{
 public:
  AntsGraph() {
    m_bCost = false;
    m_bRisk = false;
    m_maxCost = -1.;
    m_maxTime = -1.;
    m_maxRisk = -1.;
    m_riskScale = 15.;
    m_forceCount = 0;
    m_lastCost = false;
  }

  void SetOptimizeLastCost(bool b) {
    SetOptimizeCost(b);
    m_lastCost = true;
  }
  
  void SetRiskScale(double d) {
    m_riskScale = d;
  }


  void SetupFullRandom(int tasks);

  void SetOptimizeCost(bool b) {
    m_bCost = b;
    m_bRisk = false;
  }
  void SetOptimizeRisk(bool b) {
    m_bCost = false;
    m_bRisk = b;
  }


  TypeMgr & Types() {return m_types;}
  const TypeMgr & Types() const {return m_types;}

  void Read(const string & fileName);

  void Randomize();
  
  double Run(int ants);

  void Print() const;

  int isize() const {return m_nodes.isize();}
  ANode & operator[] (int i) {return m_nodes[i];}
  const ANode & operator[] (int i) const {return m_nodes[i];}
  void resize(int n) {m_nodes.resize(n);}
  void push_back(const ANode & a, bool first = true) {
    if (first)
      m_init.AddEdge(m_nodes.isize(), 0, 0, 0);
    m_nodes.push_back(a);
  }

  void AddInit(int idx) {
    m_init.AddEdge(idx, 0, 0, 0);
  }

  void SetMaxTime(double d) {
    m_maxTime = d;
  }
  void SetMaxCost(double d) {
    m_maxCost = d;
  }
  void SetMaxRisk(double d) {
    m_maxRisk = d;
  }

  void SetForce(const svec<int> & f) {
    m_forced = f;
    m_forceCount = 0;
  }

  // This is NOT the global best history!!!
  const AHistory & GetCurrentBestHistory() const {return m_bestHist;}

  int FindNode(const string & name) const {
    for (int i=0; i<m_nodes.isize(); i++) {
      if (m_nodes[i].Label() == name)
	return i;
    }
    return -1;
  }
  
 private:
  int Pick(const svec<double> & phero, const svec<int> & good); 
  svec<ANode> m_nodes;
  ANode m_init;
  bool m_bCost;
  bool m_bRisk;
  bool m_lastCost;
  
  double m_maxTime;
  double m_maxCost;
  double m_maxRisk;
  double m_riskScale;

  TypeMgr m_types;

  svec<int> m_forced;
  int m_forceCount;

  AHistory m_bestHist;

  
  int OneCycle(int ant, AHistory & hist, int curr);
  int Switch(int ant, AHistory & hist, int curr);
  double OneRun(int antnum, AHistory & hist);
  void Learn(int ants, double diff, const AHistory & hist);

 
};



#endif


