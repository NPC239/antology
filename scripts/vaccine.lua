-- dummy implementation

total = 0
last_total = 0
last_time = 0

function ants_cycle ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 1
	--print("CYCLE")
	--print("Index:    " .. index)
	--print("Time:     " .. timestamp)
	--print("Active:   " .. active)
	--print("Used:     " .. used)
	--print("Cycle:    " .. cycle)
	--print("Duration: " .. duration)
	--print("Cost:     " .. cost)
	--print("Risk:     " .. risk)
	--print("Label:    " .. label)



	if (timestamp ~= last_time) then
		last_time = timestamp
		last_total = total
		total = 0
	end


	if (used == 1) then
	        -- print("Turn off node: " .. index)
	else 
		deathrate = 1
		if (index == 2) then
			deathrate = 3
		end
		if (index == 3) then
			deathrate = 6.4
		end
		if (index == 4) then
			deathrate = 9.9
		end
		if (index == 5) then
			deathrate = 13.2
		end
		if (index == 6) then
			deathrate = 15.4
		end
		if (index == 7) then
			deathrate = 22.3
		end
			
	        --print("Last total: " .. last_total)
		cost = cost + deathrate * last_total
		
		total = total + risk
	        --print("Cost after:  " .. cost)
	end
	
	
	-- print("Risk after changed: " .. risk)
		
	return changed, duration, cost, risk
end

function ants_round_start ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 1
	total = 0
	last_total = 0
	last_time = 0
	
	cost = 0
	
	--print("")
	--print("ROUND STARTS")
		
	return changed, duration, cost, risk
end

function ants_round_end ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 0
		
	-- print("END")

	-- print("Index: " .. index)
	-- print("Time: " .. timestamp)
	-- print("Active: " .. active)
	-- print("Cycle: " .. cycle)
	-- print("Duration: " .. duration)
	-- print("Cost: " .. cost)
	-- print("Risk: " .. risk)
	-- print("Label: " .. label)
	-- risk = 0.0
	
	-- print("Risk after changed: " .. risk)
		
	return changed, duration, cost, risk
end



function mult ( x, y )
	return x * y, x + y
end


