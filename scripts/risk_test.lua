-- dummy implementation

function ants_cycle ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 1
	-- print("CYCLE")
	-- print("Index: " .. index)
	-- print("Time: " .. timestamp)
	-- print("Active: " .. active)
	-- print("Cycle: " .. cycle)
	-- print("Duration: " .. duration)
	-- print("Cost: " .. cost)
	-- print("Risk: " .. risk)
	-- print("Label: " .. label)
	-- risk = 0.333
	
	f = index/8
	f = f * timestamp
	f = f/4
	
	r = 1 - f
	r = r * ( 1 - f )
	
	risk = 1 - r
	
	-- print("Risk after changed: " .. risk)
		
	return changed, duration, cost, risk
end

function ants_round_start ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 0
		
	return changed, duration, cost, risk
end

function ants_round_end ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 1
		
	-- print("END")

	-- print("Index: " .. index)
	-- print("Time: " .. timestamp)
	-- print("Active: " .. active)
	-- print("Cycle: " .. cycle)
	-- print("Duration: " .. duration)
	-- print("Cost: " .. cost)
	-- print("Risk: " .. risk)
	-- print("Label: " .. label)
	risk = 0.0
	
	-- print("Risk after changed: " .. risk)
		
	return changed, duration, cost, risk
end



function mult ( x, y )
	return x * y, x + y
end


