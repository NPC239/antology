-- dummy implementation

last_risk = 0
last_cost = 0
last_time = 0

the_risk = 0
the_cost = 0

first = 1

function ants_cycle ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 1
	--print("CYCLE")
	--print("Index:    " .. index)
	--print("Time:     " .. timestamp)
	--print("Active:   " .. active)
	--print("Used:     " .. used)
	--print("Cycle:    " .. cycle)
	--print("Duration: " .. duration)
	--print("Cost:     " .. cost)
	--print("Risk:     " .. risk)
	--print("Label:    " .. label)



	if (timestamp ~= last_time) then
		-- print("DEBUG Risk after changed: " .. last_risk)
		-- print("DEBUG Cost after changed: " .. last_cost)
		last_time = timestamp
		the_cost = last_cost
		the_risk = last_risk
	end

	if (active == 0) then
	        -- print("Turn off node: " .. index)		
	else 
	   -- print("DEBUG Active: " .. label)
	
	   if (label == "pause") then
		last_cost = last_cost * 2.1 * (1 + last_risk)
		-- if (first == 1) then
		last_risk = last_risk * 0.7;
		first = 0
		--end
	   end
	   if (label == "aggressive") then
		last_cost = last_cost / (1.5 - last_risk/2)
		last_risk = last_risk * 1.2
	   end
	   if (label == "medium") then
		last_cost = last_cost / (1.2 - last_risk/4)
		last_risk = last_risk * 1.15
	   end
	   if (label == "light") then
		last_cost = last_cost / (1.1 - last_risk/6)
		last_risk = last_risk * 1.09
	   end
	   if (last_cost < 1) then
	   	last_cost = 1
	   end
	   if (last_risk > 0.95) then
	   	last_risk = 0.95
	   end	
	end
	
	cost = the_cost
	risk = the_risk
	

	-- print("DEBUG Label: " .. label)
	-- print("DEBUG Risk after changed: " .. risk)
	-- print("DEBUG Cost after changed: " .. cost)
		
	return changed, duration, cost, risk
end

function ants_round_start ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 1
	total = 0
	last_risk = 0.02
	last_cost = 1000
	
	the_cost = last_cost
	the_risk = last_risk
	
	first = 1
	
	cost = 1000
	risk = 0.2
	
	--print("")
	--print("ROUND STARTS")
		
	return changed, duration, cost, risk
end

function ants_round_end ( index, timestamp, active, cycle, duration, cost, risk, used, label )
	changed = 0
		
	-- print("END")

	-- print("Index: " .. index)
	-- print("Time: " .. timestamp)
	-- print("Active: " .. active)
	-- print("Cycle: " .. cycle)
	-- print("Duration: " .. duration)
	-- print("Cost: " .. cost)
	-- print("Risk: " .. risk)
	-- print("Label: " .. label)
	-- risk = 0.0
	
	-- print("Risk after changed: " .. risk)
		
	return changed, duration, cost, risk
end



function mult ( x, y )
	return x * y, x + y
end


