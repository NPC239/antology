#include "math/Matrix.h"
#include "base/FileParser.h"


void Matrix::SetSize(int x, int y)
{
  m_data.resize(x);
  for (int i=0; i<m_data.isize(); i++)
    m_data[i].resize(y, 0.);
  
}

void Matrix::Read(const string & fileName)
{
  FlatFileParser parser;
  
  parser.Open(fileName);

  
  int j = 0;
  while (parser.ParseLine()) {
    if (parser.GetItemCount() == 0)
      continue;
    if (m_data.isize() == 0)
      SetSize(parser.GetItemCount(), parser.GetItemCount());
    
    for (int i=0; i<parser.GetItemCount(); i++)
      Set(i, j, parser.AsFloat(i));

    j++;
  }
}

void Matrix::Write(const string & fileName)
{
  FILE * p = fopen(fileName.c_str(), "w");
  
  int i, j;

  for (i=0; i<X(); i++) {
    for (j=0; j<Y(); j++) {
      fprintf(p, "%f\t", Get(i, j));
    }
    fprintf(p, "\n");
  }
  
  fclose(p);
}

void Matrix::Multiply(svec<double> & d) const
{
  int i, j;

  svec<double> out;
  

  out.resize(Y(), 0.);
  for (j=0; j<Y(); j++) {
    double v = 0.;
    for (i=0; i<X(); i++) {
      v += d[i] * Get(i, j);
    }
    out[j] = v;
  }
  d = out;
}

Matrix Matrix::MatMultiply(const Matrix & m) const
{
  Matrix out;
  out.SetSize(Y(), m.X());

  //cout << "Size: " << X() << "," << Y() << endl;
  //cout << "Size: " << m.X() << "," << m.Y() << endl;
  
  int i, j, k;
  
  for (j=0; j<Y(); j++) {    
    for (i=0; i<m.X(); i++) {
      double v = 0.;
      for (k=0; k<m.Y(); k++) {
	//cout << "mul (" << k << "," << j << ")*(" << i << "," << k << ")" << endl;
	v += m.Get(i, k) * Get(k, j);	
      }
      //cout << "set (" << i << "," << j << ")" << endl;
      out.Set(i, j, v);
    }
  }

  return out;
}

void Matrix::Transpose()
{
  Matrix m;
  m.SetSize(Y(), X());

  int i, j;

  for (i=0; i<X(); i++) {
    for (j=0; j<Y(); j++) {
      //cout << i << " " << j << endl;
      m.Set(j, i, Get(i, j));
    }    
  }

  *this = m;
}

void Matrix::Print() const
{
  int i, j;
  
  for (j=0; j<Y(); j++) {    
    for (i=0; i<X(); i++) {
      cout << Get(i, j) << "\t";
    }
    cout << endl;
  }


}
