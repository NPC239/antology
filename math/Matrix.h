#ifndef MATRIX_H
#define MATRIX_H

#include "base/SVector.h"


class Matrix
{
 public:
  Matrix() {
  }
  
 
  void SetSize(int x, int y);

  void Set(int x, int y, double val) {
    (m_data[x])[y] = val;
  }

  double Get(int x, int y) const {
    return (m_data[x])[y];
  }

  int X() const {return m_data.isize(); }
  int Y() const {
    if (m_data.isize() == 0)
      return 0;
    return m_data[0].isize();
  }
  
  void Transpose();
  
  void Read(const string & fileName);
  void Write(const string & fileName);

  void Multiply(svec<double> & d) const;

  Matrix MatMultiply(const Matrix & m) const;

  void Print() const;

  const svec<double> & operator[] (int i) const {return m_data[i];}
  svec<double> & operator[] (int i) {return m_data[i];}
  
 private:
  
  svec< svec < double > > m_data;
 
};


#endif

