#include "base/JSonIO.h"
#include "base/StringUtil.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>


bool KeyVal::IsString() const
{
  //always true....
  return true;
}


bool KeyVal::IsInt() const
{
  const char * p = m_val.c_str();
  int n = strlen(p);

  for (int i=0; i<n; i++) {
    if (p[i] < '0' || p[i] > '9')
      return false;

  }
  return true;
}


bool KeyVal::IsFloat() const
{
  const char * p = m_val.c_str();
  int n = strlen(p);

  for (int i=0; i<n; i++) {
    if (p[i] < '0' || p[i] > '9') {
      if (p[i] != '.')
        return false;
    }

  }
  return true;
}


const string & KeyVal::AsString() const
{
  return m_val;
}

char KeyVal::AsChar() const 
{
  return m_val[0];
}

int KeyVal::AsInt() const
{
  return atol(m_val.c_str());
}


double KeyVal::AsFloat() const
{
  return atof(m_val.c_str());
}

void KeyVal::SetString(const string & s)
{
  m_type = 0;
  m_val = s;
}

void KeyVal::SetInt(int i)
{
  m_type = 1;
  m_val = Stringify(i);
}

void KeyVal::SetFloat(double d)
{
  m_type = 2;
  m_val = Stringify(d);
}

//==================================

void JSonIO::Read(const string & fileName)
{
  std::ifstream in(fileName); 
  in >> m_js;
}

void JSonIO::Write(const string & fileName) const
{
  std::ofstream out(fileName);
  out << m_js << std::endl;
}


svec<KeyVal> JSonIO::Get()
{
  //cout << "Get" << endl;
  svec<KeyVal> k;
  for (nlohmann::json::iterator it = m_js.begin(); it != m_js.end(); ++it) {
    KeyVal kv;
    if (!m_js[it.key()].is_array() && !m_js[it.key()].is_object()) {
      //cout << "STRING" << endl;
      kv.Value() = it.value();
    }
    //cout << "Key1" << endl;
    kv.Key() = it.key();
    //cout << it.key() << endl;
    //cout << it.value() << endl;
    k.push_back(kv);
  }
  
  return k;
}

svec<KeyVal> JSonIO::Get(const string & a)
{
  svec<KeyVal> k;
  //cout << "Enter, size = " << m_js[a].size() << endl;
  if (m_js[a].size() == 1)
    return k;
  
  for (nlohmann::json::iterator it = m_js[a].begin(); it != m_js[a].end(); ++it) {
    KeyVal kv;
    if (!m_js[a][it.key()].is_array() && !m_js[a][it.key()].is_object()) {
      //cout << "TRY" << endl;
      kv.Value() = it.value();
    }
    //cout << "Key2" << endl;
  
    kv.Key() = it.key();
    k.push_back(kv);
  }
  
  return k;
}

svec<KeyVal> JSonIO::Get(const string & a, const string & b)
{
  svec<KeyVal> k;
  if (m_js[a][b].size() == 1)
    return k;

  
  for (nlohmann::json::iterator it = m_js[a][b].begin(); it != m_js[a][b].end(); ++it) {
    KeyVal kv;
    if (!m_js[a][b].is_array() && !m_js[a][b].is_object()) {
      kv.Value() = it.value();
    }

    kv.Key() = it.key();
    k.push_back(kv);
  }
  
  return k;
}

svec<KeyVal> JSonIO::Get(const string & a, const string & b, const string & c)
{
  svec<KeyVal> k;

  if (m_js[a][b][c].size() == 1)
    return k;

  
  for (nlohmann::json::iterator it = m_js[a][b][c].begin(); it != m_js[a][b][c].end(); ++it) {
    KeyVal kv;
    if (!m_js[a][b][c].is_array() && !m_js[a][b][c].is_object()) {
      kv.Value() = it.value();
    }

    kv.Key() = it.key();
    k.push_back(kv);
  }
  
  return k;
}

void JSonIO::Set(const svec<KeyVal> & k)
{
}

void JSonIO::Set(const svec<KeyVal> & k, const string & a)
{
}

void JSonIO::Set(const svec<KeyVal> & k, const string & a, const string & b)
{
}

void JSonIO::Set(const svec<KeyVal> & k, const string & a, const string & b, const string & c)
{
}
 
