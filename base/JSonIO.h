#ifndef JSONIO_H
#define JSONIO_H

#include "base/SVector.h"
#include "extern/json/nlohmann/json.hpp"

#define KV_STRING 0
#define KV_INT 1
#define KV_FLOAT 2


class KeyVal
{
 public:
  KeyVal() {
    m_type = 0;
  }

  string & Key() {return m_key;}
  string & Value() {return m_val;}
  const string & Key() const {return m_key;}
  const string & Value() const {return m_val;}
  
  bool IsString() const;
  bool IsInt() const;
  bool IsFloat() const;


  const string & AsString() const;
  char AsChar() const;
  int AsInt() const;
  double AsFloat() const;

  void SetString(const string & s);
  void SetInt(int i);
  void SetFloat(double d);

  int Type() const {return m_type;}
  
 private:
  string m_val;
  string m_key;
  int m_type;
  
};


class JSonIO
{
 public:
  JSonIO() {}

  void Read(const string & fileName);
  void Write(const string & fileName) const;

  
  svec<KeyVal> Get();
  svec<KeyVal> Get(const string & a);
  svec<KeyVal> Get(const string & a, const string & b);
  svec<KeyVal> Get(const string & a, const string & b, const string & c);
  

  void Set(const svec<KeyVal> & k);
  void Set(const svec<KeyVal> & k, const string & a);
  void Set(const svec<KeyVal> & k, const string & a, const string & b);
  void Set(const svec<KeyVal> & k, const string & a, const string & b, const string & c);
  


  
 private:
  nlohmann::json m_js;
};


#endif //JSONIO_H
